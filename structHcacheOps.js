var structHcacheOps =
[
    [ "name", "structHcacheOps.html#ade11a3c8df80e0702ee9154cb2c04480", null ],
    [ "open", "structHcacheOps.html#a40f2826f171b6a2028e8cc2139d28c3d", null ],
    [ "fetch", "structHcacheOps.html#ac1ec741f59f23b2a8bf85be6695142e9", null ],
    [ "free", "structHcacheOps.html#af47fd374b34027f6babb25eaa1de9288", null ],
    [ "store", "structHcacheOps.html#a5df3e56b5492e3e8c244feff8c2f1d78", null ],
    [ "delete", "structHcacheOps.html#abba11681bf7c8ff0991a155a8e53dc6e", null ],
    [ "close", "structHcacheOps.html#a8a175eda3bfd2e19261226a36a9970a5", null ],
    [ "backend", "structHcacheOps.html#adfa065aeb281213b7f890b8d7c0478a1", null ]
];