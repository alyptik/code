var structSaslData =
[
    [ "saslconn", "structSaslData.html#a9a286c519449f083bebdcc676d5dfdcc", null ],
    [ "ssf", "structSaslData.html#a84cbdc39c0fa2235df10e3744a555950", null ],
    [ "pbufsize", "structSaslData.html#af7fcac395aea0184d6c0ac65ef1fadd4", null ],
    [ "buf", "structSaslData.html#a5d82e780fa213d9ed43014005a598d5c", null ],
    [ "blen", "structSaslData.html#aeb210bc38050c6ad0e2552f5bbf81ecd", null ],
    [ "bpos", "structSaslData.html#a72c3f2f8e0cd79142afb622cf9f6b71d", null ],
    [ "sockdata", "structSaslData.html#a7398f1e2ac70b23a39ec36308b405807", null ],
    [ "msasl_open", "structSaslData.html#af3c9f76828a09f17da6164b15ef47d26", null ],
    [ "msasl_close", "structSaslData.html#ab3a54763b56bc71b9bff5144ac7dd18d", null ],
    [ "msasl_read", "structSaslData.html#a0af8482b87127c953c48be71df25c7db", null ],
    [ "msasl_write", "structSaslData.html#ac32cb019109e6748435a9ec949101ea7", null ],
    [ "msasl_poll", "structSaslData.html#abc4abcd64e2b0f06227b7a5de13fcbae", null ]
];