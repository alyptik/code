var maildir_8h =
[
    [ "maildir_check_empty", "maildir_8h.html#ac1193cf5991629c477975ee74c97a65a", null ],
    [ "maildir_flags", "maildir_8h.html#aa689470d47ec79ea52a1340d753b8971", null ],
    [ "maildir_open_find_message", "maildir_8h.html#a8c1dd82fc41fced471926501135b95d4", null ],
    [ "maildir_parse_flags", "maildir_8h.html#a6b29d8d2d3aa3dcb001dc335583dff5b", null ],
    [ "maildir_parse_message", "maildir_8h.html#ae5db27589483e05b017e441c7e14a6cc", null ],
    [ "maildir_parse_stream", "maildir_8h.html#a265e3c2a1e6e4faa7a7036fd5543f9f4", null ],
    [ "maildir_update_flags", "maildir_8h.html#a17f2c230bb60cccb6720ff433b66c155", null ],
    [ "mh_mailbox", "maildir_8h.html#a7e9dcac97a8cf60fa6f0756f014fd950", null ],
    [ "mh_check_empty", "maildir_8h.html#a0bc1f221906f8f540fa7a98e81f0dc31", null ],
    [ "mx_is_maildir", "maildir_8h.html#a16b973b1ddabe2b538bdb109f8f4f880", null ],
    [ "mx_is_mh", "maildir_8h.html#abc1568e1fc35b5653e98db8470176477", null ],
    [ "mh_sync_mailbox_message", "maildir_8h.html#a5f6ec30e8d2207a60e94dc5f0d9a78a1", null ],
    [ "CheckNew", "maildir_8h.html#aa4346543f647ec8abfc9bf95c50f8d0b", null ],
    [ "MaildirHeaderCacheVerify", "maildir_8h.html#abc97746ecbcfee44cbdc5097f4b28414", null ],
    [ "MhPurge", "maildir_8h.html#ab0558ba3945d9da83c1282f8c0185f62", null ],
    [ "MhSeqFlagged", "maildir_8h.html#a79bada5434d5a65bbaf614367ce24f72", null ],
    [ "MhSeqReplied", "maildir_8h.html#a9ffb4c2cf01734483876e329e386ce56", null ],
    [ "MhSeqUnseen", "maildir_8h.html#aeadb5e6252d2df0e87fd8de23e01e240", null ],
    [ "mx_maildir_ops", "maildir_8h.html#ae819303731dd15675969a625541975cb", null ],
    [ "mx_mh_ops", "maildir_8h.html#a3ad70d603e749a60a86a3b3c3d0dd955", null ]
];