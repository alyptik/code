var rfc1524_8c =
[
    [ "rfc1524_expand_command", "rfc1524_8c.html#a660eed26810b8c8fa6dcd101f0df6555", null ],
    [ "get_field", "rfc1524_8c.html#ae97f4a4b5d7ac7eba96619f47bfc6925", null ],
    [ "get_field_text", "rfc1524_8c.html#a037c5d1655b267dd9810479759418364", null ],
    [ "rfc1524_mailcap_parse", "rfc1524_8c.html#a3bd553a9403c6846e9004802dc14a669", null ],
    [ "rfc1524_new_entry", "rfc1524_8c.html#a0599175abc299750c01cb97eebc76509", null ],
    [ "rfc1524_free_entry", "rfc1524_8c.html#a5d85b38ef1b25945fde8980bf2306d66", null ],
    [ "rfc1524_mailcap_lookup", "rfc1524_8c.html#a834f5cab63e131c8056521ae1029c942", null ],
    [ "rfc1524_expand_filename", "rfc1524_8c.html#ab4ec83386c57ef71ba37f35e1685dae3", null ],
    [ "MailcapSanitize", "rfc1524_8c.html#a8023c4f3bf53199104f5966b17bb3c73", null ]
];