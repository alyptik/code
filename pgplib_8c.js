var pgplib_8c =
[
    [ "pgp_pkalgbytype", "pgplib_8c.html#aa7eff15366fb0874ba66c05b6d656fd3", null ],
    [ "pgp_canencrypt", "pgplib_8c.html#ae3cf6bc1b74866665c1577970fffcb0a", null ],
    [ "pgp_cansign", "pgplib_8c.html#a7a081399a59a6168d26b8ed9963f1512", null ],
    [ "pgp_get_abilities", "pgplib_8c.html#a320f044d9a212dd7bcd9e81352201d24", null ],
    [ "pgp_free_uid", "pgplib_8c.html#a552eb37aa58093e6f84b85ee502130bb", null ],
    [ "pgp_copy_uids", "pgplib_8c.html#a32f7eec84948b7d944ffeabc2ab208ef", null ],
    [ "free_key", "pgplib_8c.html#aa9859b19ec23d7ac29a0173c7d39be14", null ],
    [ "pgp_remove_key", "pgplib_8c.html#a16d1e87bc88ef769630e7a6c89998674", null ],
    [ "pgp_free_key", "pgplib_8c.html#a3988a6bf657f175e77abe507c4917b5a", null ],
    [ "pgp_new_keyinfo", "pgplib_8c.html#a9dbd5f8092e073faae8a11f761fa8c04", null ]
];