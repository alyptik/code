var protos_8h =
[
    [ "mutt_set_flag", "protos_8h.html#ab71ce635c5972ab79613507e519d294c", null ],
    [ "XdgType", "protos_8h.html#a1dcac3705b40d7ef68b3b3c3f6581c0f", [
      [ "XDG_CONFIG_HOME", "protos_8h.html#a1dcac3705b40d7ef68b3b3c3f6581c0fa239f7469373769d2e133eb044b09a8b9", null ],
      [ "XDG_CONFIG_DIRS", "protos_8h.html#a1dcac3705b40d7ef68b3b3c3f6581c0fa931fb3b71248336ed08e292cbc352fd7", null ]
    ] ],
    [ "mutt_system", "protos_8h.html#aa1d4ad04b000036513ca3f741059942c", null ],
    [ "mutt_set_xdg_path", "protos_8h.html#ab34a52d61d60357a92bc3cf0da20327c", null ],
    [ "mutt_help", "protos_8h.html#a93549cef9e62d17e8bc12713ab4bcd96", null ],
    [ "mutt_make_help", "protos_8h.html#ad522bd30690ac603179ff7a4da17ef42", null ],
    [ "mutt_set_flag_update", "protos_8h.html#acd7bd1e27c93090308771ebe62a30fbf", null ],
    [ "mutt_signal_init", "protos_8h.html#a91ee7d4aca86a7dc55c9ffda2f494c37", null ],
    [ "mutt_tag_set_flag", "protos_8h.html#adfabfce0b5d8603857598a593acef8ba", null ],
    [ "mutt_change_flag", "protos_8h.html#a30d905c7406b5cda5857aec3fe9a7813", null ],
    [ "mutt_complete", "protos_8h.html#a72b3c85bc14241321c26350fd4a26b37", null ],
    [ "mutt_edit_message", "protos_8h.html#aa213ed3aebe73943fc096222c0f11485", null ],
    [ "mutt_view_message", "protos_8h.html#a6a74af0b5342a46b7aed67fc6cd01f1b", null ],
    [ "mutt_prepare_template", "protos_8h.html#ad3a5a87ecb885b8cdf4782a9e220f318", null ],
    [ "mutt_enter_string", "protos_8h.html#a2a8b6785cfa3536429d1deb6d9959e5c", null ],
    [ "mutt_enter_string_full", "protos_8h.html#a76d3af6aa66fa26ade9ea489243b5256", null ],
    [ "mutt_get_postponed", "protos_8h.html#abcaa78621be0be0446d94e7bc17453da", null ],
    [ "mutt_parse_crypt_hdr", "protos_8h.html#a9f7d1710a8c9f949b869d296094a5f4e", null ],
    [ "mutt_num_postponed", "protos_8h.html#a6ec0b4dd346e2579fb2511624c705a83", null ],
    [ "mutt_thread_set_flag", "protos_8h.html#a8dccf1de89e6d7d980fbc25f2c103eb9", null ],
    [ "mutt_update_num_postponed", "protos_8h.html#a6d6fb2f03b78be714443316f335c90ae", null ],
    [ "url_parse_mailto", "protos_8h.html#afdde3fd7b63e3baeb62575594fd49aa8", null ],
    [ "wcscasecmp", "protos_8h.html#aece8f8a0bf2359e8f8cfc6b7c9f6303f", null ],
    [ "mutt_reply_listener", "protos_8h.html#a0b922c8c9c710061346525df74154a13", null ]
];