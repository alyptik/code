var tc_8c =
[
    [ "hcache_tokyocabinet_open", "tc_8c.html#a5a4d4a5652314f5306ba246662258143", null ],
    [ "hcache_tokyocabinet_fetch", "tc_8c.html#a7b87d5f9dad1e9f804097ca23492577f", null ],
    [ "hcache_tokyocabinet_free", "tc_8c.html#a8a65f650c864a145b93d975bef54303b", null ],
    [ "hcache_tokyocabinet_store", "tc_8c.html#a5b282b53056abc3f9b6b4da4da9656ca", null ],
    [ "hcache_tokyocabinet_delete", "tc_8c.html#abc86798960e051298fb018aeab850295", null ],
    [ "hcache_tokyocabinet_close", "tc_8c.html#adc0c92862ae5c9dc86b70910ef242f9d", null ],
    [ "hcache_tokyocabinet_backend", "tc_8c.html#acde4926853daab0290a541c70d9bc4e9", null ]
];