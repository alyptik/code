var pop_8h =
[
    [ "pop_fetch_mail", "pop_8h.html#ae41e43ca791955eb76c5709db39a21d7", null ],
    [ "PopCheckinterval", "pop_8h.html#a01425c0e0398ae721cd706268e2059a2", null ],
    [ "PopDelete", "pop_8h.html#ac705d618987ca712e6eeeaf0391bb4b0", null ],
    [ "PopHost", "pop_8h.html#a6bcc0fa6b96ca7cbb969a6d44b9e226c", null ],
    [ "PopLast", "pop_8h.html#aac2c3ed29917e8cc3a4d3c49dd47463c", null ],
    [ "PopAuthenticators", "pop_8h.html#a94d9f3bd2dd44fb122234ae30bf3d17e", null ],
    [ "PopAuthTryAll", "pop_8h.html#ab1016da8fc2f6721e8fb9da2cbd5b17d", null ],
    [ "PopReconnect", "pop_8h.html#afacc929b5dc665a073ac73b15a7d1c76", null ],
    [ "mx_pop_ops", "pop_8h.html#a5e81fb63112ac5fa2310f9dab5d7f283", null ]
];