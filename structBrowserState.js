var structBrowserState =
[
    [ "entry", "structBrowserState.html#ab6759ce391f6c1f59908b8eaf2311db5", null ],
    [ "entrylen", "structBrowserState.html#a05e79f5c1a3f671286b1398d26cb52e2", null ],
    [ "entrymax", "structBrowserState.html#a077031d56ee107eba75d0e1f5e957c29", null ],
    [ "imap_browse", "structBrowserState.html#afe46fc676a2a691f2a4abbdde7c55c9b", null ],
    [ "folder", "structBrowserState.html#a103b46ca5b0d6d1c3982aef869343601", null ],
    [ "noselect", "structBrowserState.html#acb5e089e7608ba8956b08b4831e12136", null ],
    [ "marked", "structBrowserState.html#ad48ecd7441c7783c03a8e083078e5a98", null ],
    [ "unmarked", "structBrowserState.html#a786237cf7dd6570c4bbf964b6bca2489", null ]
];