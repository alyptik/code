var functions_8h =
[
    [ "OpGeneric", "functions_8h.html#a84b876b70f22a0464b06940f3bae2c49", null ],
    [ "OpMain", "functions_8h.html#acdf448ea0355dfb990218a541bc8660d", null ],
    [ "OpPager", "functions_8h.html#a01fd748de1045bed53be2b65ec25186c", null ],
    [ "OpAttach", "functions_8h.html#a6d917a9dca72419079385475a4c6af23", null ],
    [ "OpCompose", "functions_8h.html#a6dee3c280247d461f19ca099d7cc31f9", null ],
    [ "OpPost", "functions_8h.html#a666d62eb8b36e7f8f7e6be75e025af81", null ],
    [ "OpAlias", "functions_8h.html#a4b1627f18889de19a42e4e182f2d5a4d", null ],
    [ "OpBrowser", "functions_8h.html#a6a0cf03f9ed5b47836a050fc74a91409", null ],
    [ "OpQuery", "functions_8h.html#ab07c996718d028b42ce8398cbbbb289a", null ],
    [ "OpEditor", "functions_8h.html#a9c040515fe94198e9745f3e973504db1", null ],
    [ "OpPgp", "functions_8h.html#a4d7ed632136daa57f32ba356926635be", null ],
    [ "OpSmime", "functions_8h.html#a4a943332d4736f14a1a9a0efabbd6f42", null ]
];