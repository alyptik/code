var idna2_8h =
[
    [ "MI_MAY_BE_IRREVERSIBLE", "idna2_8h.html#ab40529e31f6b69f2100259d071629268", null ],
    [ "mutt_idna_intl_to_local", "idna2_8h.html#aef6b24f519e61f2fe9ae2a37bbf3ba5b", null ],
    [ "mutt_idna_local_to_intl", "idna2_8h.html#a1cf366a66462d6448ba1de79c693bf4a", null ],
    [ "mutt_idna_print_version", "idna2_8h.html#a08042c0387cf6d4ed033d36030694fec", null ],
    [ "mutt_idna_to_ascii_lz", "idna2_8h.html#ac9bf116b1719a977104fb894c52e1f49", null ],
    [ "IdnDecode", "idna2_8h.html#af72b1f7f6ae31daf9456c88def8dfaca", null ],
    [ "IdnEncode", "idna2_8h.html#a519eaee8f1c5222465d229ce7184ac68", null ]
];