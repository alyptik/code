var structAttachCtx =
[
    [ "hdr", "structAttachCtx.html#a3140f8d3ba74fb9207a63affd5aa67f5", null ],
    [ "root_fp", "structAttachCtx.html#a3cded0aa389f6219df3e80204ebe8ea8", null ],
    [ "idx", "structAttachCtx.html#a8954cda6f07f33628b3addf8bc2621fd", null ],
    [ "idxlen", "structAttachCtx.html#af02af835a67d8b3467f3a52ede4897f0", null ],
    [ "idxmax", "structAttachCtx.html#ae5d8b3d81f4cbd7a695903610ae80aa5", null ],
    [ "v2r", "structAttachCtx.html#af3d254a510a44c1a2f55eef4a27908fc", null ],
    [ "vcount", "structAttachCtx.html#a0dfc8b92b72351536f7bd3cfbc08eb30", null ],
    [ "fp_idx", "structAttachCtx.html#ace417205b7b51e4e1aecc9670ee8aa34", null ],
    [ "fp_len", "structAttachCtx.html#a99439756ab72e5a9ca59b3ee74d0260e", null ],
    [ "fp_max", "structAttachCtx.html#a7ccc5aab103dffe2142c01d9d2d8faed", null ],
    [ "body_idx", "structAttachCtx.html#a91b62fe3fb812a138c34f793b6a543d6", null ],
    [ "body_len", "structAttachCtx.html#a2a640066cd23ee29a8608ff2755526fc", null ],
    [ "body_max", "structAttachCtx.html#ab1383f18f861b863f091d29fa41c5e1d", null ]
];