var lmdb_8c =
[
    [ "HcacheLmdbCtx", "structHcacheLmdbCtx.html", "structHcacheLmdbCtx" ],
    [ "MdbTxnMode", "lmdb_8c.html#aeecdc6fc084a2a3646e242f0e0343514", [
      [ "TXN_UNINITIALIZED", "lmdb_8c.html#aeecdc6fc084a2a3646e242f0e0343514a8967a2f6cedb6773ac00ede97267f710", null ],
      [ "TXN_READ", "lmdb_8c.html#aeecdc6fc084a2a3646e242f0e0343514a5ff52c31fd17555eee58e59be657e29a", null ],
      [ "TXN_WRITE", "lmdb_8c.html#aeecdc6fc084a2a3646e242f0e0343514a8336ebb46e8f3a6c3f2147bcdfda5edc", null ]
    ] ],
    [ "mdb_get_r_txn", "lmdb_8c.html#ac8e418654228e57187bce74203410e4a", null ],
    [ "mdb_get_w_txn", "lmdb_8c.html#a6b897b9ed0a54cace6c0cfa3abcb243a", null ],
    [ "hcache_lmdb_open", "lmdb_8c.html#afe046483d92b9eca16e32fc2d54deb5b", null ],
    [ "hcache_lmdb_fetch", "lmdb_8c.html#a6a78720d896832ffc29d89642cc2f444", null ],
    [ "hcache_lmdb_free", "lmdb_8c.html#ad27e8b2d3e3648185f9a5c6eb2b06113", null ],
    [ "hcache_lmdb_store", "lmdb_8c.html#a7acc550ab5976b818b80e30d914b291c", null ],
    [ "hcache_lmdb_delete", "lmdb_8c.html#ab15172ece83d85b7a511003c33b1fcfe", null ],
    [ "hcache_lmdb_close", "lmdb_8c.html#a2716ebbe91a8c90d294ac35c87b8dc14", null ],
    [ "hcache_lmdb_backend", "lmdb_8c.html#a93984504d5fd2a7251b73c6c72997bc7", null ],
    [ "LMDB_DB_SIZE", "lmdb_8c.html#a7b7e505a32baece1f9d9b6d2a89620bb", null ]
];