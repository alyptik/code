var conn__globals_8h =
[
    [ "ConnectTimeout", "conn__globals_8h.html#a883fed5877f48718db7cc65e959cf869", null ],
    [ "CertificateFile", "conn__globals_8h.html#a614465af7c5735247e797aef5b5712d8", null ],
    [ "EntropyFile", "conn__globals_8h.html#a9c3e6a8099809bd2d9737464446752b8", null ],
    [ "SslCiphers", "conn__globals_8h.html#a89df611584b3acbe3d030a030b6ffd56", null ],
    [ "SslClientCert", "conn__globals_8h.html#a24a4859cc50d09ba0700297d7bda9cd8", null ],
    [ "SslCaCertificatesFile", "conn__globals_8h.html#af68a5e2e1fcc4d25aa2c201199bc67a2", null ],
    [ "SslMinDhPrimeBits", "conn__globals_8h.html#a1841b342f20ddc1578c52fcf8b80fa12", null ],
    [ "Preconnect", "conn__globals_8h.html#af5b1aca5fc60d49f91e5c721e9024fa2", null ],
    [ "Tunnel", "conn__globals_8h.html#aba84b1a95c82090d9c81fdcb418336b4", null ],
    [ "UseIpv6", "conn__globals_8h.html#ae03830b1115e6fad5f5a3ea9caa988fc", null ],
    [ "SslUseSslv3", "conn__globals_8h.html#a385c6ded1b17c068656df38b9251ac82", null ],
    [ "SslUseTlsv1", "conn__globals_8h.html#a42968c1769397229b0775118ba010b60", null ],
    [ "SslUseTlsv11", "conn__globals_8h.html#a77b30a0dba2766da19f74349d09bc5a6", null ],
    [ "SslUseTlsv12", "conn__globals_8h.html#a1d8dc1880bd8e30511d2898463f5f84c", null ],
    [ "SslVerifyDates", "conn__globals_8h.html#a9789e58184f7f994b85c05cf57a59995", null ],
    [ "SslVerifyHost", "conn__globals_8h.html#a68c00997c359fa10c8cb73a072c6f04c", null ]
];