var pop__private_8h =
[
    [ "PopCache", "structPopCache.html", "structPopCache" ],
    [ "PopData", "structPopData.html", "structPopData" ],
    [ "PopAuth", "structPopAuth.html", "structPopAuth" ],
    [ "POP_PORT", "pop__private_8h.html#ac8bd6d29115abf7dcc47ae26f0cbb3d6", null ],
    [ "POP_SSL_PORT", "pop__private_8h.html#a2e7c786c53a65d43284b8de44a98a5fc", null ],
    [ "POP_CACHE_LEN", "pop__private_8h.html#a606d455144b12dcddd75503a8fb737a4", null ],
    [ "POP_CMD_RESPONSE", "pop__private_8h.html#a320026da03c4cc9c4b7a15a78dd77568", null ],
    [ "pop_query", "pop__private_8h.html#a6038df03ea685d868ebc7cb52196859a", null ],
    [ "PopStatus", "pop__private_8h.html#abf5ce360d246b1f6d637d953c182ea39", [
      [ "POP_NONE", "pop__private_8h.html#abf5ce360d246b1f6d637d953c182ea39a9202803328d8645fb7e7dcdb69b52eff", null ],
      [ "POP_CONNECTED", "pop__private_8h.html#abf5ce360d246b1f6d637d953c182ea39a073a47f9fccbec3754108f10b4647c48", null ],
      [ "POP_DISCONNECTED", "pop__private_8h.html#abf5ce360d246b1f6d637d953c182ea39a05f59c3c4b782d93659d94ca5df16b7d", null ],
      [ "POP_BYE", "pop__private_8h.html#abf5ce360d246b1f6d637d953c182ea39aca46adc14d7f12e84d5f7b5cb80407fe", null ]
    ] ],
    [ "PopAuthRes", "pop__private_8h.html#a4bca7d3fe1a979439caa3f4d1b561f50", [
      [ "POP_A_SUCCESS", "pop__private_8h.html#a4bca7d3fe1a979439caa3f4d1b561f50a4f778fe4bd10a07201dcad7c81e354b6", null ],
      [ "POP_A_SOCKET", "pop__private_8h.html#a4bca7d3fe1a979439caa3f4d1b561f50af9fcefd3c03ab36585a3da1f7da071ae", null ],
      [ "POP_A_FAILURE", "pop__private_8h.html#a4bca7d3fe1a979439caa3f4d1b561f50a601657443e0e9ce9255943a120377fdc", null ],
      [ "POP_A_UNAVAIL", "pop__private_8h.html#a4bca7d3fe1a979439caa3f4d1b561f50a04de03803dd743f762a3b33d5a5ba739", null ]
    ] ],
    [ "pop_authenticate", "pop__private_8h.html#a20cf4ea869ec23f8877e1d574854ac11", null ],
    [ "pop_apop_timestamp", "pop__private_8h.html#a85766822bc38633eb1889ab8f100457d", null ],
    [ "pop_parse_path", "pop__private_8h.html#a701e7f371e5551a9a68d94905e4bc6b5", null ],
    [ "pop_connect", "pop__private_8h.html#a0f40c74560784c4b32de155263463631", null ],
    [ "pop_open_connection", "pop__private_8h.html#a9c34e1890bd21e4def6b67b744fe8cc3", null ],
    [ "pop_query_d", "pop__private_8h.html#ac9ca9dfff58a7689a148f46d39fdbcb6", null ],
    [ "pop_fetch_data", "pop__private_8h.html#ac904f3343289681bd5500b928cc243e9", null ],
    [ "pop_reconnect", "pop__private_8h.html#a4d17074436b0e0565956ecb300d28cf2", null ],
    [ "pop_logout", "pop__private_8h.html#ac142cb53cf1915efb5a5c84b59ed7878", null ]
];