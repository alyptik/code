var rfc1524_8h =
[
    [ "Rfc1524MailcapEntry", "structRfc1524MailcapEntry.html", "structRfc1524MailcapEntry" ],
    [ "rfc1524_new_entry", "rfc1524_8h.html#a0599175abc299750c01cb97eebc76509", null ],
    [ "rfc1524_free_entry", "rfc1524_8h.html#a5d85b38ef1b25945fde8980bf2306d66", null ],
    [ "rfc1524_expand_command", "rfc1524_8h.html#a660eed26810b8c8fa6dcd101f0df6555", null ],
    [ "rfc1524_expand_filename", "rfc1524_8h.html#ab4ec83386c57ef71ba37f35e1685dae3", null ],
    [ "rfc1524_mailcap_lookup", "rfc1524_8h.html#a834f5cab63e131c8056521ae1029c942", null ],
    [ "MailcapSanitize", "rfc1524_8h.html#a8023c4f3bf53199104f5966b17bb3c73", null ]
];