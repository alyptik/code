var tunnel_8c =
[
    [ "TunnelData", "structTunnelData.html", "structTunnelData" ],
    [ "tunnel_socket_open", "tunnel_8c.html#afbb9829f3f988c86ff1a01d9c9ea6e03", null ],
    [ "tunnel_socket_close", "tunnel_8c.html#a583380c9cf4ff2c49eeb9e54f6cfcad3", null ],
    [ "tunnel_socket_read", "tunnel_8c.html#a5216391d7da81fe039f80a1cc5a50819", null ],
    [ "tunnel_socket_write", "tunnel_8c.html#a3b3907a9edb3eb93904689f17e3da42b", null ],
    [ "tunnel_socket_poll", "tunnel_8c.html#aef546927ab07168c0f1d85ecbd202bf6", null ],
    [ "mutt_tunnel_socket_setup", "tunnel_8c.html#a1fb9c0c719b86c3d07726f560454a5b6", null ]
];