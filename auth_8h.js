var auth_8h =
[
    [ "ImapAuth", "structImapAuth.html", "structImapAuth" ],
    [ "ImapAuthRes", "auth_8h.html#aba20e35487bf461507e63b783243dbad", [
      [ "IMAP_AUTH_SUCCESS", "auth_8h.html#aba20e35487bf461507e63b783243dbada544df33a559dae35b9b71951782eb897", null ],
      [ "IMAP_AUTH_FAILURE", "auth_8h.html#aba20e35487bf461507e63b783243dbada4e84acd157fdc566eafb0f8963a179f2", null ],
      [ "IMAP_AUTH_UNAVAIL", "auth_8h.html#aba20e35487bf461507e63b783243dbadaddf2ec71f2502b2e4684bfe42fc2fd40", null ]
    ] ],
    [ "imap_auth_plain", "auth_8h.html#a29a95145f0e0e5c19d2d8e085cff9aba", null ],
    [ "imap_auth_login", "auth_8h.html#abb26a091e960e3d234ca8d3e33700d62", null ],
    [ "imap_auth_gss", "auth_8h.html#a013a06b2578393579585808f9763e83b", null ],
    [ "imap_auth_sasl", "auth_8h.html#a20df19153f0a84479482001b8f911099", null ]
];