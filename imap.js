var imap =
[
    [ "IMAP network mailbox", "imap_imap.html", null ],
    [ "IMAP anonymous authentication method", "imap_auth_anon.html", null ],
    [ "IMAP authenticator multiplexor", "imap_auth.html", null ],
    [ "IMAP CRAM-MD5 authentication method", "imap_auth_cram.html", null ],
    [ "IMAP GSS authentication method", "imap_auth_gss.html", null ],
    [ "IMAP login authentication method", "imap_auth_login.html", null ],
    [ "IMAP plain authentication method", "imap_auth_plain.html", null ],
    [ "IMAP SASL authentication method", "imap_auth_sasl.html", null ],
    [ "Mailbox browser", "imap_browse.html", null ],
    [ "Send/receive commands to/from an IMAP server", "imap_command.html", null ],
    [ "Manage IMAP messages", "imap_message.html", null ],
    [ "UTF-7 Manipulation", "imap_utf7.html", null ],
    [ "IMAP helper functions", "imap_util.html", null ]
];