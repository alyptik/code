var structNntpData =
[
    [ "group", "structNntpData.html#a6283880e7891eb089f871e92b9e9a6d0", null ],
    [ "desc", "structNntpData.html#afd106f439b5ccabc451c26e15d6aecdf", null ],
    [ "first_message", "structNntpData.html#a25842f531ce3542e57d64d011501f1f9", null ],
    [ "last_message", "structNntpData.html#a6f0b84604c845f0a40a2f95a0985d814", null ],
    [ "last_loaded", "structNntpData.html#a1aad56c6ba4fbfd9e57efcf8e90b5996", null ],
    [ "last_cached", "structNntpData.html#a3d7df8151b38b56beb1acabd85c768d3", null ],
    [ "unread", "structNntpData.html#a2dedbc4d0eef1a39ff429df8a7d80d94", null ],
    [ "subscribed", "structNntpData.html#a62f9d52fb95c3b6612429ee5b42b85af", null ],
    [ "new", "structNntpData.html#a9296baa739492ba3a4b75eb541506133", null ],
    [ "allowed", "structNntpData.html#a5630623f6913de73ae493a2b943baff8", null ],
    [ "deleted", "structNntpData.html#a7e6dc6aca0064de84cd3062717348714", null ],
    [ "newsrc_len", "structNntpData.html#aaad304b406bc44e963c9bc02ac7d4a9a", null ],
    [ "newsrc_ent", "structNntpData.html#a5fd143851de749de23a8588f5857cec3", null ],
    [ "nserv", "structNntpData.html#acee2efdefa364ef20beddbede0931efe", null ],
    [ "acache", "structNntpData.html#a3d5b178b7e70a23c5afd7dbdfeff6946", null ],
    [ "bcache", "structNntpData.html#ac3d2eddda6e2b61f84adc15556091f76", null ]
];