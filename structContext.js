var structContext =
[
    [ "path", "structContext.html#a037124f7f9a6385389c79863b8279256", null ],
    [ "realpath", "structContext.html#a1595f4d8121f11827e8b026e66d28bb1", null ],
    [ "fp", "structContext.html#a088b00286a9ab2d65f083b6cf11ed88f", null ],
    [ "atime", "structContext.html#a858ffc673091cdfb341931851b4801b4", null ],
    [ "mtime", "structContext.html#a9c6efccecf4164446bc4cbef98aeb2ad", null ],
    [ "size", "structContext.html#adacb398014eaedc67722ffabc42a4bf8", null ],
    [ "vsize", "structContext.html#a252a82eaae91091309c8691a8950b066", null ],
    [ "pattern", "structContext.html#a146acecc03ea332cc3aa2aa00bb2f341", null ],
    [ "limit_pattern", "structContext.html#a1ade119bdb296ccb5a5d9a89bb8ddba1", null ],
    [ "hdrs", "structContext.html#a88a88fa14e12d0720fd056813702cc61", null ],
    [ "last_tag", "structContext.html#a51473838fa79a8a72da1377298358b66", null ],
    [ "tree", "structContext.html#a1ef5680fd22f0a913f489c11f3271a42", null ],
    [ "id_hash", "structContext.html#adfa343fa1883c9e8b076dd7e8ea24ffe", null ],
    [ "subj_hash", "structContext.html#a688189589019b75ffff17a085256131a", null ],
    [ "thread_hash", "structContext.html#a8cc199b129eaf8e3f646b04b2595be6f", null ],
    [ "label_hash", "structContext.html#a6678b249caf0f3fa61a80be2d8d12edf", null ],
    [ "v2r", "structContext.html#a7c6aab5bb88c41c0c14591cd6220acd0", null ],
    [ "hdrmax", "structContext.html#a47f657c30a75329f884502bd4b530849", null ],
    [ "msgcount", "structContext.html#a7326e0d1a27479fe505e1e38ff06cff5", null ],
    [ "vcount", "structContext.html#a3ce92ffd9a07688dec80ebfc5a433644", null ],
    [ "tagged", "structContext.html#aaad5ec419ebe0af8987fc75c670e9836", null ],
    [ "new", "structContext.html#a085d21512c0891bdae715eff29d890c2", null ],
    [ "unread", "structContext.html#a0f11bc9f90e3e36ef1186edfbed23563", null ],
    [ "deleted", "structContext.html#afbd44374d8a29bf5f1bbbe737a23cf29", null ],
    [ "flagged", "structContext.html#a63b683454c9893285909845f77eccefb", null ],
    [ "msgnotreadyet", "structContext.html#a52f2d78e97dc68a292e1ad4256d4b0df", null ],
    [ "menu", "structContext.html#a50cd7ee4bdbd3c662858ee5c4da4b079", null ],
    [ "magic", "structContext.html#a1860fa40ac0e7994381d61aa9a6c0e39", null ],
    [ "rights", "structContext.html#a779e3d476c66dd99be15e588c24808b1", null ],
    [ "locked", "structContext.html#a2a78cae40a21b6e425213377855d91ed", null ],
    [ "changed", "structContext.html#ade8d2a9deb4ac636be1e958f47966697", null ],
    [ "readonly", "structContext.html#a9a0201ffbf48d007b2b6c268ad7f4467", null ],
    [ "dontwrite", "structContext.html#af9a554d7095b82ca96113ffc2a9ab971", null ],
    [ "append", "structContext.html#aeaa11e395e282b4f8852f6c6af106700", null ],
    [ "quiet", "structContext.html#a5ad97e2cad023acfee498232911471b6", null ],
    [ "collapsed", "structContext.html#a4e50eec66cb620aefa315dcef071ac7f", null ],
    [ "closing", "structContext.html#aad46a353527b09c99a62b5b36cb78c6d", null ],
    [ "peekonly", "structContext.html#a596769f312828775e0aa137f02abe33c", null ],
    [ "compress_info", "structContext.html#a869f12030678273fbb305ba77bb435a8", null ],
    [ "data", "structContext.html#a56e5416e6ab6e31b8c54aef00be39319", null ],
    [ "mx_ops", "structContext.html#a2c0d69f3cd710bc755b5385cf281f49e", null ]
];