var config_2quad_8c =
[
    [ "quad_string_set", "config_2quad_8c.html#a5fbe281ad243031e17574df9cacb7d7e", null ],
    [ "quad_string_get", "config_2quad_8c.html#a574c775adea19be4655175b2fbbc8c38", null ],
    [ "quad_native_set", "config_2quad_8c.html#aaaeca6a9d90ddda8278427e87b53beb8", null ],
    [ "quad_native_get", "config_2quad_8c.html#a6d66064192ce84f02aedd76fdcbeaf22", null ],
    [ "quad_reset", "config_2quad_8c.html#a2a994f2c2d681cbbea8bdc2af86696cc", null ],
    [ "quad_init", "config_2quad_8c.html#ade096ee3fb64386af67bd4b664066506", null ],
    [ "quad_toggle", "config_2quad_8c.html#a68786c45b09b6fb886fcecb2f479fde4", null ],
    [ "quad_he_toggle", "config_2quad_8c.html#af386d93cecc2e776c0dd470390a71b85", null ],
    [ "quad_values", "config_2quad_8c.html#acffd35b2a242f653744936f3228150b3", null ]
];