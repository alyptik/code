var alias_8h =
[
    [ "Alias", "structAlias.html", "structAlias" ],
    [ "TAILQ_HEAD", "alias_8h.html#ac8b2a934090d6906291ff2772ee26dc9", null ],
    [ "mutt_alias_create", "alias_8h.html#a95a1f5c567eca51458f60800bef47c84", null ],
    [ "mutt_alias_free", "alias_8h.html#a367a44e27dff0ed9545beec6de4e1dcb", null ],
    [ "mutt_aliaslist_free", "alias_8h.html#aab5ced5fa11da534b01c4a1cbedbdfad", null ],
    [ "mutt_alias_lookup", "alias_8h.html#ab5a91bf6ab1fcd9342e90f9da30a4954", null ],
    [ "mutt_expand_aliases_env", "alias_8h.html#aa90fd4b086aebbf4fed5245ecaca1ac1", null ],
    [ "mutt_expand_aliases", "alias_8h.html#aea3d897a501d63b3cad3f8a6bbc119da", null ],
    [ "mutt_get_address", "alias_8h.html#a0d2597deb568afc71c8328d3b324eea4", null ],
    [ "mutt_addr_is_user", "alias_8h.html#a1a7905f34ac21a48a34e14e2c1becbef", null ],
    [ "mutt_alias_complete", "alias_8h.html#a300d6d4b88787e97d446a714501a992f", null ],
    [ "mutt_alias_add_reverse", "alias_8h.html#af897edddf49e2f9e8432b88763d47952", null ],
    [ "mutt_alias_delete_reverse", "alias_8h.html#a4dd6ae37df1048c70f0b9d0caaaab124", null ],
    [ "mutt_alias_reverse_lookup", "alias_8h.html#a6d1a2b0ced7c75c54b9e651b04b88eaf", null ]
];