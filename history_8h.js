var history_8h =
[
    [ "HistoryClass", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234", [
      [ "HC_CMD", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234aff1655e936d39511e2576e9eebd7aba3", null ],
      [ "HC_ALIAS", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234a7087d625c6e65aa739171b10a68907c5", null ],
      [ "HC_COMMAND", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234a3cda685722d8561b4d28aa4fad865007", null ],
      [ "HC_FILE", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234a313ffe25b1e9a86bb1f9afb44e15925c", null ],
      [ "HC_PATTERN", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234aede85bc3dcca6122e349576277dcef4b", null ],
      [ "HC_OTHER", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234aee9379d265cad600929abe5cc271019a", null ],
      [ "HC_MBOX", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234a268178c32488395d8afeac98117a88ee", null ],
      [ "HC_LAST", "history_8h.html#acb0a49d94e5b4fe512c30e2c12621234a25f434722d44d1a864774530964ac131", null ]
    ] ],
    [ "mutt_hist_add", "history_8h.html#a541c9c8be36706c87b62c896a897f680", null ],
    [ "mutt_hist_at_scratch", "history_8h.html#a493d2064e68bab2fe5cee4b4769c2d49", null ],
    [ "mutt_hist_free", "history_8h.html#a7074ec85134f8fec1a104f95a07ded2f", null ],
    [ "mutt_hist_init", "history_8h.html#a46801529f80d425b727d1d4a3fc57a97", null ],
    [ "mutt_hist_next", "history_8h.html#a3bd0e4a7ce28aae28736c86f577968b9", null ],
    [ "mutt_hist_prev", "history_8h.html#a9c81cb82877d80de3874bc359c473d12", null ],
    [ "mutt_hist_read_file", "history_8h.html#a282fdccadb31bf6aaf2a45c392a46f88", null ],
    [ "mutt_hist_reset_state", "history_8h.html#a6197a536a3da0385551bac50a45d6a7a", null ],
    [ "mutt_hist_save_scratch", "history_8h.html#a2d26cab55cd51b83f6e2a45a8ce44799", null ],
    [ "mutt_hist_search", "history_8h.html#a005f30de35d49e92f06bfb4289722dce", null ],
    [ "mutt_hist_listener", "history_8h.html#a1f1a70774ac41ec1a7faed984dd608f4", null ],
    [ "History", "history_8h.html#a9e7541211242353202a75be0be4893d3", null ],
    [ "HistoryFile", "history_8h.html#aa320f7f4652bfab3c52b3a668591b0f6", null ],
    [ "HistoryRemoveDups", "history_8h.html#a6c77dfd520f64fbc389a6a4699938966", null ],
    [ "SaveHistory", "history_8h.html#a4db3aad9ff8ea8a30ed75d1fbdeb611c", null ]
];