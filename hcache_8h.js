var hcache_8h =
[
    [ "HeaderCache", "structHeaderCache.html", "structHeaderCache" ],
    [ "Validate", "unionValidate.html", "unionValidate" ],
    [ "header_cache_t", "hcache_8h.html#a22450ae394b396a27772fa32c73fa468", null ],
    [ "hcache_namer_t", "hcache_8h.html#a7e5559b4225e00e82ebf12ce8329dbf1", null ],
    [ "mutt_hcache_open", "hcache_8h.html#ae01c81d7bc2f290309a28bbb8474ff40", null ],
    [ "mutt_hcache_close", "hcache_8h.html#ab70197e91a184083ea2ac9d4d75c47f0", null ],
    [ "mutt_hcache_fetch", "hcache_8h.html#a328c1cf43151360a9001b2423ffd6ab8", null ],
    [ "mutt_hcache_fetch_raw", "hcache_8h.html#ae65c9e1141287d9e21e541475df3f7bf", null ],
    [ "mutt_hcache_free", "hcache_8h.html#a51e16ac36abe59d7df4b6be113efe90c", null ],
    [ "mutt_hcache_restore", "hcache_8h.html#a556940057108b6e02ff298c2aabe4e6a", null ],
    [ "mutt_hcache_store", "hcache_8h.html#a2506fd544924cac6eea25644d7057872", null ],
    [ "mutt_hcache_store_raw", "hcache_8h.html#add0651518cff8eb01d398664cd98037b", null ],
    [ "mutt_hcache_delete", "hcache_8h.html#af464c9289099045a67095846de0939de", null ],
    [ "mutt_hcache_backend_list", "hcache_8h.html#a19989208cbd44d4083d872702017e1fc", null ],
    [ "mutt_hcache_is_valid_backend", "hcache_8h.html#af2522d53121a164856680743be32ea18", null ],
    [ "HeaderCacheBackend", "hcache_8h.html#a741812afe210d1b16e38e53fe62bca16", null ]
];