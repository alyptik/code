var structMxOps =
[
    [ "mbox_open", "structMxOps.html#a5ada4919bbadbf81fbe610e2194dd9b9", null ],
    [ "mbox_open_append", "structMxOps.html#a270445d15bd4e2e0d9bafec801166e81", null ],
    [ "mbox_check", "structMxOps.html#a571ea136ddc840c1a88695e630ff723d", null ],
    [ "mbox_sync", "structMxOps.html#aa4da846ac86263f07129943c063c520d", null ],
    [ "mbox_close", "structMxOps.html#a2b08bfd7c0b3824cbe936280b37e585e", null ],
    [ "msg_open", "structMxOps.html#ac6b6cfde6016ad7d19c6e716f5f35742", null ],
    [ "msg_open_new", "structMxOps.html#abfd8ed4b06f385dc519d8d1459d5fa8e", null ],
    [ "msg_commit", "structMxOps.html#a136345fdc9b49f87c6fa002b71cc3151", null ],
    [ "msg_close", "structMxOps.html#af193fab2627a893df61b6dcd3039b6e1", null ],
    [ "tags_edit", "structMxOps.html#a90892198d789675f88f9dd0c78b8d18f", null ],
    [ "tags_commit", "structMxOps.html#aeceeaf3e4165139f9483be9ff8a83ce5", null ]
];