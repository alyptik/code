var gdbm_8c =
[
    [ "hcache_gdbm_open", "gdbm_8c.html#a0378d48fdefb88d95b820786d1f7d1ee", null ],
    [ "hcache_gdbm_fetch", "gdbm_8c.html#a5f4b132924d2133b5d495866d00db0d1", null ],
    [ "hcache_gdbm_free", "gdbm_8c.html#a943b85846456ad7626655bec565aafad", null ],
    [ "hcache_gdbm_store", "gdbm_8c.html#a6bdc35464221395063daf4fa92a5e09d", null ],
    [ "hcache_gdbm_delete", "gdbm_8c.html#a233917d4f4cdded9a751da4c0b4487b2", null ],
    [ "hcache_gdbm_close", "gdbm_8c.html#a2a832c6b9abf3bb408c876811d4f3124", null ],
    [ "hcache_gdbm_backend", "gdbm_8c.html#a7310d88b15be6cd156ac2252f74e452c", null ]
];