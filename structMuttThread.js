var structMuttThread =
[
    [ "fake_thread", "structMuttThread.html#a68156e7ac570e245272ba291dfd840fd", null ],
    [ "duplicate_thread", "structMuttThread.html#add9fafa2e72810244df9eee34eff25fa", null ],
    [ "sort_children", "structMuttThread.html#a1fb485f333eb5dbc8b4e74196b818906", null ],
    [ "check_subject", "structMuttThread.html#ae942fadad384d962155370650dd56890", null ],
    [ "visible", "structMuttThread.html#a30c80a1167887f0dc4ab7bff443b8df3", null ],
    [ "deep", "structMuttThread.html#a8c1fd0aa931c50f735bec5e5660856b8", null ],
    [ "subtree_visible", "structMuttThread.html#abfc3f860f57e8c763cbcdc586b8b1448", null ],
    [ "next_subtree_visible", "structMuttThread.html#ae8aeb3656c0937883a1a0c955b12712c", null ],
    [ "parent", "structMuttThread.html#a39d5bb1957cb943617225ff5efb9f55c", null ],
    [ "child", "structMuttThread.html#a7067492dadd86f81def9036474086211", null ],
    [ "next", "structMuttThread.html#aee5a1652f34056b764d12844e15f4345", null ],
    [ "prev", "structMuttThread.html#a5931c5905063b72935f0b37f044dad00", null ],
    [ "message", "structMuttThread.html#a03b9c328675fab7a259798638d2671a3", null ],
    [ "sort_key", "structMuttThread.html#ac37facc2eed7ce57fd38494b9add67c3", null ]
];