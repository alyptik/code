var group_8c =
[
    [ "mutt_pattern_group", "group_8c.html#a155eff28c896fa67f4c316902c2a3fb9", null ],
    [ "group_remove", "group_8c.html#a3ad7cc65031fa1174d8fddb060095661", null ],
    [ "mutt_group_context_clear", "group_8c.html#a8660c3d7becb5bef2c3725019e4f9950", null ],
    [ "empty_group", "group_8c.html#a09783c5a45915df055e9e126ad9a8d7f", null ],
    [ "mutt_group_context_add", "group_8c.html#a9a6795c02f6aaaec4d026a40878398a1", null ],
    [ "mutt_group_context_destroy", "group_8c.html#ab72e9a6349098240dfa2257789226cc4", null ],
    [ "group_add_addrlist", "group_8c.html#aedb6af6c44f0b159f266b101038551a5", null ],
    [ "group_remove_addrlist", "group_8c.html#adc82711a9e54ceb81b78c702bae43ab8", null ],
    [ "group_add_regex", "group_8c.html#a3dec0dc31400b69197ffd45839e4b0df", null ],
    [ "group_remove_regex", "group_8c.html#a9ce5c79bfae1e6c071f44f4770177fad", null ],
    [ "mutt_group_context_add_addrlist", "group_8c.html#ab532b0aac627bf0b1536a6868e1ab471", null ],
    [ "mutt_group_context_remove_addrlist", "group_8c.html#acc0f422c0d51c27ecd4e34e2aa2c2aec", null ],
    [ "mutt_group_context_add_regex", "group_8c.html#ab13fe1dc2aabed615c1e1770626f283a", null ],
    [ "mutt_group_context_remove_regex", "group_8c.html#afd7a739e11c90470771dabe35e4adad7", null ],
    [ "mutt_group_match", "group_8c.html#a85d4636eab11d9c91cc6af8840819f86", null ]
];