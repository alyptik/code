var config_2sort_8c =
[
    [ "sort_string_set", "config_2sort_8c.html#ac9cb1b1a69665a0b25f880c16d786730", null ],
    [ "sort_string_get", "config_2sort_8c.html#af458194ca4d2919a626af268cb81cd0f", null ],
    [ "sort_native_set", "config_2sort_8c.html#ad4ee67ea5d8ab1c9d654fc2e8fa77af8", null ],
    [ "sort_native_get", "config_2sort_8c.html#a18be22df2e15ec86a710d846e548a8b6", null ],
    [ "sort_reset", "config_2sort_8c.html#a46f3b85af79f492736ace05a9665986a", null ],
    [ "sort_init", "config_2sort_8c.html#a950a5552f7fa1af85327fac1fe894c9f", null ],
    [ "SortAliasMethods", "config_2sort_8c.html#a6accc5f32e32ca3daa7c85ceeb133106", null ],
    [ "SortAuxMethods", "config_2sort_8c.html#ac7f3faefbd4de2a855ebe35e07867f95", null ],
    [ "SortBrowserMethods", "config_2sort_8c.html#a62d1200a7833a80c1b44631fc91ac874", null ],
    [ "SortKeyMethods", "config_2sort_8c.html#ac7317ba9b6d4e63e5385d9d2fca6d461", null ],
    [ "SortMethods", "config_2sort_8c.html#a212caaae7a78c55728bc774f190e8948", null ],
    [ "SortSidebarMethods", "config_2sort_8c.html#ae0911cae0349cea33d5f5ce960d22cef", null ]
];