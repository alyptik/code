var send_8c =
[
    [ "append_signature", "send_8c.html#acfddae7b85ce28e1472b6020a1673f25", null ],
    [ "mutt_remove_xrefs", "send_8c.html#abf823e2372ec4bb456d9e6e34d787356", null ],
    [ "remove_user", "send_8c.html#a8dbe8fe3aaa8670c489f20ddd3429b19", null ],
    [ "find_mailing_lists", "send_8c.html#a0a1b0ba3d7dfce4a440003a0ef8d1406", null ],
    [ "edit_address", "send_8c.html#aaab226dd1e48ef2ae0e5e7c49ed893d9", null ],
    [ "edit_envelope", "send_8c.html#aa5e9fbc05824f7864dad9246248183fc", null ],
    [ "nntp_get_header", "send_8c.html#a1f444e7c2a2266af7e16fade94f07841", null ],
    [ "process_user_recips", "send_8c.html#a93b53146e2fbfb6657f83297e722fdac", null ],
    [ "process_user_header", "send_8c.html#a9bca5c9f44a7908485389d16dcf20f96", null ],
    [ "mutt_forward_intro", "send_8c.html#aac4aed1c0c37099f13e9afa9f7e4717a", null ],
    [ "mutt_forward_trailer", "send_8c.html#af456fd1ca84c554df4308c41581ac411", null ],
    [ "include_forward", "send_8c.html#a0a0be08e79d9956ae69693f80c2ea54f", null ],
    [ "mutt_make_attribution", "send_8c.html#a12e02dfbb566ed25f64133efa9772552", null ],
    [ "mutt_make_post_indent", "send_8c.html#a8726131809116d461feaac1348439bab", null ],
    [ "include_reply", "send_8c.html#a41e3fbbc7857aef9878e86e6a26ec064", null ],
    [ "default_to", "send_8c.html#aaec5fb925baf7e91165e27f6677977ee", null ],
    [ "mutt_fetch_recips", "send_8c.html#a4d715fd0327fde01ebbab857a3a31fbf", null ],
    [ "add_references", "send_8c.html#a977b299077aadc1dbb202969e9298088", null ],
    [ "add_message_id", "send_8c.html#a4ea5cbb7f73277e25d100414329ccb89", null ],
    [ "mutt_fix_reply_recipients", "send_8c.html#a9804f2dbaf0406ea6803ba5a304aeab6", null ],
    [ "mutt_make_forward_subject", "send_8c.html#afd1ce7628fe8d960740c07f8e2937e4b", null ],
    [ "mutt_make_misc_reply_headers", "send_8c.html#a666a7c4e10ccd2d38264a05ebc6409ee", null ],
    [ "mutt_add_to_reference_headers", "send_8c.html#a3bd32443d49b53466af32d101c16d418", null ],
    [ "make_reference_headers", "send_8c.html#a4dc1db31fa9f20289bbfd6d04e4b94f5", null ],
    [ "envelope_defaults", "send_8c.html#a9bb1ac9097cd8ddb93a26b50d05feddc", null ],
    [ "generate_body", "send_8c.html#a78319c70a25a901804c6fa76cb5cbe37", null ],
    [ "mutt_set_followup_to", "send_8c.html#add6775201da3c94fc20b47af6d1ff03e", null ],
    [ "set_reverse_name", "send_8c.html#aceaf8e10fec5b4b11d95495d68114067", null ],
    [ "mutt_default_from", "send_8c.html#aa57b6946b9f64b289da0da408ee27789", null ],
    [ "send_message", "send_8c.html#ae6a53a87d2adfab9e338964ca78e1f6f", null ],
    [ "mutt_encode_descriptions", "send_8c.html#ad242f4b2f11d8b765d7a74c522a7f82b", null ],
    [ "decode_descriptions", "send_8c.html#af1b54154700865964598299ed1cf9a6a", null ],
    [ "fix_end_of_file", "send_8c.html#ad5e73f6a253107a7f55ee7388fbbf92f", null ],
    [ "mutt_compose_to_sender", "send_8c.html#af73a105695bcdd42869729b39deeae3e", null ],
    [ "mutt_resend_message", "send_8c.html#a9e554a8b485b628ccfeaa97810043c85", null ],
    [ "is_reply", "send_8c.html#ae9ba6ef90293e4520a944e226adc87c2", null ],
    [ "search_attach_keyword", "send_8c.html#a2d7b04f6fbef1138284497b8a2764e85", null ],
    [ "ci_send_message", "send_8c.html#a09c909ab2bb41101590ed2b13f65fc2d", null ],
    [ "AbortNoattach", "send_8c.html#a10149d7b7e6079a0d31941e78dec30ee", null ],
    [ "AbortNoattachRegex", "send_8c.html#a95efdfe4314fbdfd5949f71b6cda8656", null ],
    [ "AbortNosubject", "send_8c.html#a6ddb1c7448cc2a75f6a345cefd2c7def", null ],
    [ "AbortUnmodified", "send_8c.html#ae7cb728b1f65d77d5ec469ec8966e987", null ],
    [ "AskFollowUp", "send_8c.html#aa99007ca682e898ccb444fb645c5da06", null ],
    [ "AskXCommentTo", "send_8c.html#ac9af836d03abeff552f101661c09f755", null ],
    [ "ContentType", "send_8c.html#ad2acc4859a0c850c18ce27c46c4d412c", null ],
    [ "CryptAutoencrypt", "send_8c.html#af20a8f1368cf17243cdd9ee7c7b6a976", null ],
    [ "CryptAutopgp", "send_8c.html#a0fc54beef6868e211518d1591d66e465", null ],
    [ "CryptAutosign", "send_8c.html#aef6173005f979798e6e0df98c9198f8c", null ],
    [ "CryptAutosmime", "send_8c.html#a0720d4c3eda8e800747c03aed1d7a880", null ],
    [ "CryptReplyencrypt", "send_8c.html#ac4c612f7a3f980d8bd2f02c58065b006", null ],
    [ "CryptReplysign", "send_8c.html#ac980de08dfd3ba8a5741e74bf8fc20f0", null ],
    [ "CryptReplysignencrypted", "send_8c.html#a45e987391047d52b53211eb51db07fdd", null ],
    [ "EmptySubject", "send_8c.html#acc5d988611fa74c16bfda95543eeb0a7", null ],
    [ "FastReply", "send_8c.html#a5c02ccb6066a60c3cf823cdc4d07e9a0", null ],
    [ "FccAttach", "send_8c.html#abdb557a5f3376258c5a820ba41888f61", null ],
    [ "FccClear", "send_8c.html#a032f674513c9ffcb5f1688e78ef1493c", null ],
    [ "FollowupTo", "send_8c.html#aab339d67d602b7a60890019f08f5bf8e", null ],
    [ "ForwardAttributionIntro", "send_8c.html#a401ddaf13ad42e8f482e21c24d41ef73", null ],
    [ "ForwardAttributionTrailer", "send_8c.html#ac3e7b2be79a4797baae2f8f9740473cd", null ],
    [ "ForwardEdit", "send_8c.html#acc65081ed6430cd302efc145167ad383", null ],
    [ "ForwardFormat", "send_8c.html#ada8f95eb7497c6cb92101ea82643ee89", null ],
    [ "ForwardReferences", "send_8c.html#a33ce9ba9c1b4d29b3e47376d12b1f08a", null ],
    [ "Hdrs", "send_8c.html#a82eeca8c10ee05a7ef0ee6efdc2d7ff9", null ],
    [ "HonorFollowupTo", "send_8c.html#a96da15bf81b8a9412121c4b415bb72a6", null ],
    [ "IgnoreListReplyTo", "send_8c.html#a83bf88cced0475aa2e83b1153f518828", null ],
    [ "Include", "send_8c.html#a255b5f0e21c4f3bfed6937eddfe6f684", null ],
    [ "Metoo", "send_8c.html#a18d82b8af1578388e05412aa2f2bfb22", null ],
    [ "NmRecord", "send_8c.html#a23467f2dfe0255936996e36b2e99164e", null ],
    [ "PgpReplyinline", "send_8c.html#aa8f3191c0c030f64ea9b3f80776910b1", null ],
    [ "PostIndentString", "send_8c.html#a3f736328be5622ad48cce675b7448ed3", null ],
    [ "PostponeEncrypt", "send_8c.html#a8f8144a5145ae8df39a72669d5b81f7b", null ],
    [ "PostponeEncryptAs", "send_8c.html#a9c708821432e345c81b35b8a7a77dbd7", null ],
    [ "Recall", "send_8c.html#ab3ddd30a2bb5b715f4e0298c02d7e96f", null ],
    [ "ReplySelf", "send_8c.html#aabff870432738b0debbb68b432b64ecd", null ],
    [ "ReplyTo", "send_8c.html#a5c4db7ad549922a43d6a0ca64acb4c3d", null ],
    [ "ReplyWithXorig", "send_8c.html#a60d7228b6980ca86fd597b8aef6b2d68", null ],
    [ "ReverseName", "send_8c.html#a7f8b307454612856239b0a7f1bd21e79", null ],
    [ "ReverseRealname", "send_8c.html#a67f7490cd74b54e7a3a7dc3b9dc5db0e", null ],
    [ "SigDashes", "send_8c.html#ad5106e71900af27e938cdd3beb73b8fe", null ],
    [ "Signature", "send_8c.html#a0555c9cd51fe81d2348233a27a649206", null ],
    [ "SigOnTop", "send_8c.html#a4253c1ce47c4c1c41c87db4557e6c4c7", null ],
    [ "UseFrom", "send_8c.html#a178d218a453a5baa8e72fffa6a068938", null ]
];