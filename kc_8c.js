var kc_8c =
[
    [ "hcache_kyotocabinet_open", "kc_8c.html#ab46125504921d47d3c95c639da0318ee", null ],
    [ "hcache_kyotocabinet_fetch", "kc_8c.html#a9fdabbb0bb214c3f951893873e2cb770", null ],
    [ "hcache_kyotocabinet_free", "kc_8c.html#a888446989ef93fefc36d30bc240faa06", null ],
    [ "hcache_kyotocabinet_store", "kc_8c.html#a44f33fe980c0b35a7d2f27221b10b96d", null ],
    [ "hcache_kyotocabinet_delete", "kc_8c.html#a48a66b793d4b577cb7f7a6df77eeb0fd", null ],
    [ "hcache_kyotocabinet_close", "kc_8c.html#a4143c1da07cbd5a7fa4a69fc4d10a9ec", null ],
    [ "hcache_kyotocabinet_backend", "kc_8c.html#ab53492cba17d3eaa91c1928f14daa62d", null ]
];