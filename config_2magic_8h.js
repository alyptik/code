var config_2magic_8h =
[
    [ "MailboxType", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921d", [
      [ "MUTT_MAILBOX_ERROR", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da33fa729dbeb1a8b678fd94effae0bdc9", null ],
      [ "MUTT_UNKNOWN", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921daf14aa0761bb43e33a26f31276a5cd608", null ],
      [ "MUTT_MBOX", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da817ef3cf5f529965875400b412678525", null ],
      [ "MUTT_MMDF", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da09cef4c4e3dc42c9137860413dcdb374", null ],
      [ "MUTT_MH", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da54a635e509630de2bfd6fd6f46e9fd89", null ],
      [ "MUTT_MAILDIR", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921dafd4cdaa9851e1c936b2f648b4bdc6ad4", null ],
      [ "MUTT_NNTP", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da6d3955b4b5acc450ba154b6ec9100803", null ],
      [ "MUTT_IMAP", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da7f541a03089370b1a260e344f8d1809b", null ],
      [ "MUTT_NOTMUCH", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da05c2d20da557ac5b2c45a349eec1fba6", null ],
      [ "MUTT_POP", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921da3b55e432fcbfdbd550d0aea5b03679de", null ],
      [ "MUTT_COMPRESSED", "config_2magic_8h.html#af65addf714c28dd5d483cb426100921dabe3e473aa651d6b7030b5ef08953eb4e", null ]
    ] ],
    [ "magic_init", "config_2magic_8h.html#aea18668b6c1e82da15a57082eeee24a7", null ],
    [ "magic_values", "config_2magic_8h.html#aa9cc24ef09cda690853a23248b1608d6", null ]
];