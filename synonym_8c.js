var synonym_8c =
[
    [ "TEST_NO_MAIN", "synonym_8c.html#a1b84ff28c89e01d51a2312861b012599", null ],
    [ "test_string_set", "synonym_8c.html#a6481c1ea806f91ee55dacd7bfe01acd8", null ],
    [ "test_string_get", "synonym_8c.html#a85fd1e4e98bab2f2185f2d6cb270d14b", null ],
    [ "test_native_set", "synonym_8c.html#ab5935bf604cc953008fde261e13163b8", null ],
    [ "test_native_get", "synonym_8c.html#afdd5f411dfa0e2d1c7c7a2a9710182dd", null ],
    [ "test_reset", "synonym_8c.html#ac8ddbbe16d88cb9b096c15cf225cbeb3", null ],
    [ "config_synonym", "synonym_8c.html#acf81082ee377e802e06c0e4533c5d90f", null ],
    [ "VarApple", "synonym_8c.html#a9c18bdcae4abdd1a9525578e5aa5d0a7", null ],
    [ "VarCherry", "synonym_8c.html#aa5336ec90481656d3147fc722ee50ba6", null ],
    [ "VarElderberry", "synonym_8c.html#ab5dc47ef77d4551c81348ded3718f368", null ],
    [ "VarGuava", "synonym_8c.html#a184d1c2444582c30dc7c7bc34a874a25", null ],
    [ "VarIlama", "synonym_8c.html#ae7e5d9ff37bdf56c7d1102c19560c615", null ],
    [ "Vars", "synonym_8c.html#a0ec6714905325f37533da49c4f419268", null ],
    [ "Vars2", "synonym_8c.html#a4e38d1312b41b6bae44f5f81ee6f77e9", null ]
];