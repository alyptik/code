var config_2quad_8h =
[
    [ "QuadOption", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35db", [
      [ "MUTT_ABORT", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35dbaaa75b5db14c0aed1fde665034f113339", null ],
      [ "MUTT_NO", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35dbabe3800cc419f25384aaf1892fc3c812b", null ],
      [ "MUTT_YES", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35dbafee7088606c4724651c799a6a0b182b7", null ],
      [ "MUTT_ASKNO", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35dbaaf90604c0e1430d5002944c93ac46952", null ],
      [ "MUTT_ASKYES", "config_2quad_8h.html#a738f558a271fe5035e0ee3b700ee35dbae15a0a03ca0d129a7ff4530a73fea140", null ]
    ] ],
    [ "quad_init", "config_2quad_8h.html#ade096ee3fb64386af67bd4b664066506", null ],
    [ "quad_he_toggle", "config_2quad_8h.html#af386d93cecc2e776c0dd470390a71b85", null ],
    [ "quad_values", "config_2quad_8h.html#acffd35b2a242f653744936f3228150b3", null ]
];