var structImapStatus =
[
    [ "name", "structImapStatus.html#afb76e08141fe4671f575288ac1c8d312", null ],
    [ "messages", "structImapStatus.html#aea88a4f3e56a491d8556eb40ec705874", null ],
    [ "recent", "structImapStatus.html#a1b5112800d42cb86271a11679eac05c8", null ],
    [ "uidnext", "structImapStatus.html#a767c6e6d2a0eb9b3bfca9654f379eb90", null ],
    [ "uidvalidity", "structImapStatus.html#a89af28c19355ccdacaebf673a7b0e4e6", null ],
    [ "unseen", "structImapStatus.html#ac06d340313422cb9617899554f66ab34", null ]
];