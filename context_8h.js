var context_8h =
[
    [ "Context", "structContext.html", "structContext" ],
    [ "AclRights", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfb", [
      [ "MUTT_ACL_LOOKUP", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfbae5c64b7fc9b75021c1b1b155296114cb", null ],
      [ "MUTT_ACL_READ", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba949c1ee1b71cf7eb5dad6b1332be1cd0", null ],
      [ "MUTT_ACL_SEEN", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfbac3c89a989df1b6ce02047d2bde266616", null ],
      [ "MUTT_ACL_WRITE", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba5998f3e34aec2c9372c03ff076f4ff46", null ],
      [ "MUTT_ACL_INSERT", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba617bbf49f435cfcc203680ed4465e3d2", null ],
      [ "MUTT_ACL_POST", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba95bb95a4901cdad234d1904c3005cb61", null ],
      [ "MUTT_ACL_CREATE", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba9b94d4715561b4bda93fd008a14b22d9", null ],
      [ "MUTT_ACL_DELMX", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba729e51eb504a9bc85eee080c8722fabc", null ],
      [ "MUTT_ACL_DELETE", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfbab96d23d41e901084f957168329d353b6", null ],
      [ "MUTT_ACL_EXPUNGE", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfbabefb5b957fc70326fc8d17aacf422ab1", null ],
      [ "MUTT_ACL_ADMIN", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba78ff5c83255b558cb00a4cc9f15c039d", null ],
      [ "RIGHTSMAX", "context_8h.html#aacd2e9c61c416687deb73a9f0877cdfba8c03907b81632bf0f4222f4067567498", null ]
    ] ]
];