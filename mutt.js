var mutt =
[
    [ "Conversion to/from base64 encoding", "base64.html", null ],
    [ "General purpose object for storing and parsing strings", "buffer.html", null ],
    [ "Conversion between different character encodings", "charset.html", null ],
    [ "Time and date handling routines", "date.html", null ],
    [ "Private copy of the environment variables", "envlist.html", null ],
    [ "Leave the program NOW", "exit.html", null ],
    [ "File management functions", "file.html", null ],
    [ "Hash table data structure", "hash.html", null ],
    [ "Read/write command history from/to a file", "history.html", null ],
    [ "Singly-linked list type", "list.html", null ],
    [ "Logging Dispatcher", "logging.html", null ],
    [ "Map between a string and a constant", "mapping.html", null ],
    [ "Multi-byte String manipulation functions", "mbyte.html", null ],
    [ "Calculate the MD5 checksum of a buffer", "md5.html", null ],
    [ "Memory management wrappers", "memory.html", null ],
    [ "Path manipulation functions", "path.html", null ],
    [ "Manage regular expressions", "regex.html", null ],
    [ "Calculate the SHA1 checksum of a buffer", "sha1.html", null ],
    [ "Signal handling", "signal.html", null ],
    [ "String manipulation functions", "string.html", null ]
];