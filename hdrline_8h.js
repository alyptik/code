var hdrline_8h =
[
    [ "HdrFormatInfo", "structHdrFormatInfo.html", "structHdrFormatInfo" ],
    [ "mutt_make_string", "hdrline_8h.html#a4143626473a79495586956e53f187a14", null ],
    [ "mutt_is_mail_list", "hdrline_8h.html#a791acc95459b326738b870ac0b5d607d", null ],
    [ "mutt_is_subscribed_list", "hdrline_8h.html#a643450005e6bb0522c8ea4db46cfe39e", null ],
    [ "mutt_make_string_flags", "hdrline_8h.html#a6de0878ccf20f84d785878dad420eef0", null ],
    [ "mutt_make_string_info", "hdrline_8h.html#afc3550eab1c41d947cc3d33634af2b6f", null ],
    [ "FlagChars", "hdrline_8h.html#a784ef265bfb34b8d21a129aa461821dc", null ],
    [ "FromChars", "hdrline_8h.html#a4fb2e90f19121ead77806bc3f1c447cc", null ],
    [ "ToChars", "hdrline_8h.html#af95dba2a52233a603b7eecc921cbef42", null ]
];