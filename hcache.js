var hcache =
[
    [ "Email-object serialiser", "hc_serial.html", null ],
    [ "Header cache multiplexor", "hc_hcache.html", null ],
    [ "Berkeley DB", "hc_bdb.html", null ],
    [ "GDMB", "hc_gdbm.html", null ],
    [ "Kyoto Cabinet", "hc_kc.html", null ],
    [ "LMDB", "hc_lmdb.html", null ],
    [ "QDBM", "hc_qdbm.html", null ],
    [ "Tokyo Cabinet", "hc_tc.html", null ]
];