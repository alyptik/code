var structRfc1524MailcapEntry =
[
    [ "command", "structRfc1524MailcapEntry.html#addff0b45c83051ae015a5bf93ba73c73", null ],
    [ "testcommand", "structRfc1524MailcapEntry.html#a1ff1cb1c5ff58617f1910836e415a9f5", null ],
    [ "composecommand", "structRfc1524MailcapEntry.html#a8322043ab69e09a9f2ddd850c27c48f2", null ],
    [ "composetypecommand", "structRfc1524MailcapEntry.html#a84bd5736bfae5bde8f4f64679c19685e", null ],
    [ "editcommand", "structRfc1524MailcapEntry.html#ac68896f677a326d7d0bc13d43bb64849", null ],
    [ "printcommand", "structRfc1524MailcapEntry.html#a4d470ce1746adf7dae7542420f57173a", null ],
    [ "nametemplate", "structRfc1524MailcapEntry.html#a2e2b7908fbe8ea50851f8f7f9585c925", null ],
    [ "convert", "structRfc1524MailcapEntry.html#a2336e57e6d5e3619b0d584c361231449", null ],
    [ "needsterminal", "structRfc1524MailcapEntry.html#a8c83a3a0a76c16f809af6d56d6ba1bda", null ],
    [ "copiousoutput", "structRfc1524MailcapEntry.html#af1f49b092918b7997cb717c7c00f4236", null ]
];