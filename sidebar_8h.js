var sidebar_8h =
[
    [ "mutt_sb_change_mailbox", "sidebar_8h.html#aec201a8e5376d56ce51d3285edbe1f5b", null ],
    [ "mutt_sb_draw", "sidebar_8h.html#a7154f9e9258668fc2a578df7cac5f33a", null ],
    [ "mutt_sb_get_highlight", "sidebar_8h.html#a592fd7fecc3aa208731b245a68a3918c", null ],
    [ "mutt_sb_notify_mailbox", "sidebar_8h.html#af8717f4e2a6696790301765d28801db1", null ],
    [ "mutt_sb_set_mailbox_stats", "sidebar_8h.html#a091d9cbd858c315c6331b308c16ba39b", null ],
    [ "mutt_sb_set_open_mailbox", "sidebar_8h.html#a5468a6fb6731c33f796928f35ebde3c7", null ],
    [ "mutt_sb_toggle_virtual", "sidebar_8h.html#aec376b713f59fd13f4d3787cd50492b6", null ],
    [ "SidebarComponentDepth", "sidebar_8h.html#ae63ec05b2dbb0f975cfe5a0d81cfa427", null ],
    [ "SidebarDelimChars", "sidebar_8h.html#aafbcfc55259fb832a20be9a99892bd63", null ],
    [ "SidebarDividerChar", "sidebar_8h.html#aee1c9270b865daa82bb07fbe6684f827", null ],
    [ "SidebarFolderIndent", "sidebar_8h.html#a42de55bd5bef2892c945581a93a4db9b", null ],
    [ "SidebarFormat", "sidebar_8h.html#aa74954e527f863d0d8903d09a9778aa2", null ],
    [ "SidebarIndentString", "sidebar_8h.html#aeaf1b55ab44f480ecd4669e23308e38b", null ],
    [ "SidebarNewMailOnly", "sidebar_8h.html#aea41c210d074f96bf6e92e8cae6014e5", null ],
    [ "SidebarNextNewWrap", "sidebar_8h.html#aee05151df39fbb4b9a4e3e4adeae5673", null ],
    [ "SidebarShortPath", "sidebar_8h.html#a27eed6fd1200412beb8ae85a6da22a02", null ],
    [ "SidebarSortMethod", "sidebar_8h.html#a97e7ea23e88962d03b48afad2f70e3f7", null ]
];