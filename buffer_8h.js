var buffer_8h =
[
    [ "Buffer", "structBuffer.html", "structBuffer" ],
    [ "MoreArgs", "buffer_8h.html#a4b6d76229fd106b97ef690d31b0d0afe", null ],
    [ "mutt_buffer_add", "buffer_8h.html#a4e84e8698806eef6eec1ad4a880537e0", null ],
    [ "mutt_buffer_addch", "buffer_8h.html#a819a9cb669c1a7e8e2218c672b794492", null ],
    [ "mutt_buffer_addstr", "buffer_8h.html#a958e87c1ae9de976eb76f759dbaaa601", null ],
    [ "mutt_buffer_alloc", "buffer_8h.html#a5928463d232a1bec2803510cb2798e2f", null ],
    [ "mutt_buffer_free", "buffer_8h.html#a4fe44d6951505adb974135817a85a3d1", null ],
    [ "mutt_buffer_from", "buffer_8h.html#a2dbbf64489c1ea71237cfa83bcf50f13", null ],
    [ "mutt_buffer_init", "buffer_8h.html#ab31dc6030636f826c60c65d143cb249d", null ],
    [ "mutt_buffer_is_empty", "buffer_8h.html#a2d7c4a32422377609be66b0f620c4971", null ],
    [ "mutt_buffer_new", "buffer_8h.html#a8e45cf675c7eeedae5b614402f2e84b5", null ],
    [ "mutt_buffer_printf", "buffer_8h.html#ad057ee239ae5bcfd72cab3749036ab02", null ],
    [ "mutt_buffer_reset", "buffer_8h.html#a8da54c4ec8e1a4e21158f134441bdf2d", null ]
];