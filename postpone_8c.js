var postpone_8c =
[
    [ "mutt_num_postponed", "postpone_8c.html#a6ec0b4dd346e2579fb2511624c705a83", null ],
    [ "mutt_update_num_postponed", "postpone_8c.html#a6d6fb2f03b78be714443316f335c90ae", null ],
    [ "post_entry", "postpone_8c.html#a092862dd7ebf63ee3f788751a7ae478a", null ],
    [ "select_msg", "postpone_8c.html#a52c3666adbfb95453e29046ed90eb57f", null ],
    [ "mutt_get_postponed", "postpone_8c.html#abcaa78621be0be0446d94e7bc17453da", null ],
    [ "mutt_parse_crypt_hdr", "postpone_8c.html#a9f7d1710a8c9f949b869d296094a5f4e", null ],
    [ "mutt_prepare_template", "postpone_8c.html#ad3a5a87ecb885b8cdf4782a9e220f318", null ],
    [ "PostponeHelp", "postpone_8c.html#a3e424dff2a008c512d0639bbfcd4867c", null ],
    [ "PostCount", "postpone_8c.html#a813a6eba284cc0eb882c66dcc4a9ba31", null ],
    [ "PostContext", "postpone_8c.html#ac3248206e253d6ac8f90d19beef8e570", null ],
    [ "UpdateNumPostponed", "postpone_8c.html#aed051d98e3ce57d157314a4bab61956f", null ]
];