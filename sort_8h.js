var sort_8h =
[
    [ "SORTCODE", "sort_8h.html#a8c9da77aca7f22e76b2677714100628d", null ],
    [ "sort_t", "sort_8h.html#a52c34c488cc5fe13653fa49e323bc253", null ],
    [ "mutt_get_sort_func", "sort_8h.html#a86d2a4de61fab225b10c9e9a7bae01db", null ],
    [ "mutt_sort_headers", "sort_8h.html#aa99d4b590a29f24c0950934c15be6a83", null ],
    [ "perform_auxsort", "sort_8h.html#a1d69cc6b2dfd825821b49d541edf5ca1", null ],
    [ "mutt_get_name", "sort_8h.html#a454decb89368ace74106e17470e11113", null ],
    [ "ReverseAlias", "sort_8h.html#abdff29c6023f9dd2fb93d2889391ac6e", null ],
    [ "SortMethods", "sort_8h.html#a212caaae7a78c55728bc774f190e8948", null ],
    [ "Sort", "sort_8h.html#a650b05c7c84efbaa19b929619c52de39", null ],
    [ "SortAux", "sort_8h.html#a2347ed7e90a3dd504f92a5b76b0ba843", null ],
    [ "PgpSortKeys", "sort_8h.html#aa27122dd072ae78de042be31219e13d1", null ]
];