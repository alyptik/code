var nntp__private_8h =
[
    [ "NNTP_PORT", "nntp__private_8h.html#aaccdd399473f113674cbabfeaa5ef972", null ],
    [ "NNTP_SSL_PORT", "nntp__private_8h.html#a1e2acdcc22ea61457b4dcafc8825aae5", null ],
    [ "NHDR", "nntp__private_8h.html#ae025b51045d28c7582a64abae7ee591e", null ],
    [ "NntpStatus", "nntp__private_8h.html#a30e5c5a44ecbd929699e93d887f39f6f", [
      [ "NNTP_NONE", "nntp__private_8h.html#a30e5c5a44ecbd929699e93d887f39f6fa32f5431a095a439134751f874712176c", null ],
      [ "NNTP_OK", "nntp__private_8h.html#a30e5c5a44ecbd929699e93d887f39f6faf2f388bc874e63dbdd37734a53e9ac03", null ],
      [ "NNTP_BYE", "nntp__private_8h.html#a30e5c5a44ecbd929699e93d887f39f6fa3c8db6bef2f46a4076cb9888ac96a1a0", null ]
    ] ],
    [ "nntp_acache_free", "nntp__private_8h.html#ab32d7a61b019ef0b8a5ee9297bf262c4", null ],
    [ "nntp_active_save_cache", "nntp__private_8h.html#a5b96613c43f16181b7dfd4ed56fe8018", null ],
    [ "nntp_add_group", "nntp__private_8h.html#a4a81ebd8f2d0f68109bddeef2c87ea83", null ],
    [ "nntp_bcache_update", "nntp__private_8h.html#a8cd4bc8be6b60e3a8606aecf7e7c4649", null ],
    [ "nntp_check_new_groups", "nntp__private_8h.html#a8d2eba7519328d548ebd9f811410e62e", null ],
    [ "nntp_data_free", "nntp__private_8h.html#ac6f4ccee68fe9c806a1cef452a580e1f", null ],
    [ "nntp_delete_group_cache", "nntp__private_8h.html#a82191bedc739c0685de995b1914a4e7c", null ],
    [ "nntp_group_unread_stat", "nntp__private_8h.html#aaf40cb308522ba644def1a4ac59935b0", null ],
    [ "nntp_newsrc_gen_entries", "nntp__private_8h.html#a0ee4304a87ae1fcb1615d1259e5fd3a0", null ],
    [ "nntp_open_connection", "nntp__private_8h.html#a864c34fdab5ddf5d793c021bbfc57d0e", null ],
    [ "nntp_hcache_open", "nntp__private_8h.html#a54b77657020d208a5f223206fdc776d0", null ],
    [ "nntp_hcache_update", "nntp__private_8h.html#a842977310a0307ba3a754ee049d1d77f", null ]
];