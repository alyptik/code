var mbox_8c =
[
    [ "MUpdate", "structMUpdate.html", "structMUpdate" ],
    [ "mbox_lock_mailbox", "mbox_8c.html#a957a425a523a9efbc032584673e03400", null ],
    [ "mbox_unlock_mailbox", "mbox_8c.html#af5126acdb9c8ddd9583a645342f0a3b4", null ],
    [ "mmdf_parse_mailbox", "mbox_8c.html#ae1efd8707efa96b7e0b8daf0105024ee", null ],
    [ "mbox_parse_mailbox", "mbox_8c.html#ad72dff92f5cf9ec13c2819dbcf9e0c9b", null ],
    [ "mbox_mbox_open", "mbox_8c.html#abb852d85f42cd12ec22682a3e18529c5", null ],
    [ "mbox_mbox_open_append", "mbox_8c.html#a87463d807cbf08ceb7498da410ade187", null ],
    [ "mbox_mbox_close", "mbox_8c.html#a65e4a96f0322341067e8ae40d812b2cf", null ],
    [ "mbox_msg_open", "mbox_8c.html#a2a533262209e4a3da720e453bbdd6fea", null ],
    [ "mbox_msg_close", "mbox_8c.html#a7eca81f8017aa6d9e09364fa9c0880d5", null ],
    [ "mbox_msg_commit", "mbox_8c.html#aae3c67ccfe2e9786a4eb16ee31ce2ba0", null ],
    [ "mmdf_msg_commit", "mbox_8c.html#a4b52c34670dad7dcc51836a8f496b228", null ],
    [ "mbox_msg_open_new", "mbox_8c.html#a423637a1329dac4a6fb05554fd61a4fd", null ],
    [ "reopen_mailbox", "mbox_8c.html#a0f186f813b5573085e88e111de1c46a0", null ],
    [ "mbox_mbox_check", "mbox_8c.html#a1774543272b536cb9c62e9135fefdffa", null ],
    [ "mbox_has_new", "mbox_8c.html#a651289ff3c25a5985bb817bbc2880b33", null ],
    [ "mbox_reset_atime", "mbox_8c.html#a1ba2731e95f7ca4476ec601cba8a1dc5", null ],
    [ "mbox_mbox_sync", "mbox_8c.html#ac3bd73b0ad1b3a39f62c9fd00544e4ce", null ],
    [ "mx_mbox_ops", "mbox_8c.html#ac7607a05a9b274b40f08883245f679e4", null ],
    [ "mx_mmdf_ops", "mbox_8c.html#a14ba5353cc7b2d448a15a8ec1cc049d0", null ]
];