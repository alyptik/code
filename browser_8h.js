var browser_8h =
[
    [ "FolderFile", "structFolderFile.html", "structFolderFile" ],
    [ "BrowserState", "structBrowserState.html", "structBrowserState" ],
    [ "MUTT_SEL_MAILBOX", "browser_8h.html#a0264d8194c2a80ca40416bb661d3cd27", null ],
    [ "MUTT_SEL_MULTI", "browser_8h.html#a3c8fc96890e5df4412f83fbc2a87cc91", null ],
    [ "MUTT_SEL_FOLDER", "browser_8h.html#a3bdfd6d174ddcbbd6488554080d54889", null ],
    [ "MUTT_SEL_VFOLDER", "browser_8h.html#afcb6a506141b271b49e0892c3648451c", null ],
    [ "mutt_select_file", "browser_8h.html#ab036990112b8014c0b84c401e77f96d3", null ],
    [ "mutt_browser_select_dir", "browser_8h.html#acb6bbfb5afda49517fada811752944ff", null ],
    [ "BrowserAbbreviateMailboxes", "browser_8h.html#a4c63613bdbac44a4e46e98c4440c032a", null ],
    [ "FolderFormat", "browser_8h.html#a4d8716d54e42df9097e1ae72c0a0d5e3", null ],
    [ "GroupIndexFormat", "browser_8h.html#ac9a2c4763aa721e78d4a34a871cbeb2a", null ],
    [ "NewsgroupsCharset", "browser_8h.html#a9dbc86163de88e52cd6c6b2838981c59", null ],
    [ "ShowOnlyUnread", "browser_8h.html#aeaf1085e67916b960d7d0d0569eb8a82", null ],
    [ "SortBrowser", "browser_8h.html#afbb427047ffd011fdd7774ea28944f25", null ],
    [ "VfolderFormat", "browser_8h.html#ae3c7a47f35805dcf780e215c7743a5f9", null ]
];