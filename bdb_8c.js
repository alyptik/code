var bdb_8c =
[
    [ "HcacheDbCtx", "structHcacheDbCtx.html", "structHcacheDbCtx" ],
    [ "dbt_init", "bdb_8c.html#ac0d858225d9c67a81344e4fdb79ef132", null ],
    [ "dbt_empty_init", "bdb_8c.html#a8ba0de880d78ebd26fb9c0e2f793d75d", null ],
    [ "hcache_bdb_open", "bdb_8c.html#a1364ea04345d5183bff7e05927356a67", null ],
    [ "hcache_bdb_fetch", "bdb_8c.html#a92b41b15cb4b728f20839e52fb87e4b4", null ],
    [ "hcache_bdb_free", "bdb_8c.html#adea066dd5157e6b92b7a39ead0f1d612", null ],
    [ "hcache_bdb_store", "bdb_8c.html#ad9bd972f891518801a0d933eec941f83", null ],
    [ "hcache_bdb_delete", "bdb_8c.html#a00b245d1c48d528bb643ca582f1e7e79", null ],
    [ "hcache_bdb_close", "bdb_8c.html#ae2bf412cd8eb66fcb1620d1a03803c36", null ],
    [ "hcache_bdb_backend", "bdb_8c.html#a47a16fda825b0ef3b116f763e02d6e50", null ]
];