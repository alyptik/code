var hcache_8c =
[
    [ "HCACHE_BACKEND", "hcache_8c.html#a19daca2082a8add642e698602b625fb2", null ],
    [ "hcache_get_ops", "hcache_8c.html#a51511cd14787b537b38ed24431384743", null ],
    [ "hcache_get_backend_ops", "hcache_8c.html#af820150342f1bb1c41cb8add852212b1", null ],
    [ "crc_matches", "hcache_8c.html#aa36d7ffb56dd4652348e2e75bed65137", null ],
    [ "create_hcache_dir", "hcache_8c.html#a5086e15744da558388677cc362f74959", null ],
    [ "hcache_per_folder", "hcache_8c.html#a14d516f0472a9e5e1102d5ad724580fe", null ],
    [ "get_foldername", "hcache_8c.html#a950cdaf6e000a670ba816ef87bca445e", null ],
    [ "mutt_hcache_open", "hcache_8c.html#ae01c81d7bc2f290309a28bbb8474ff40", null ],
    [ "mutt_hcache_close", "hcache_8c.html#ab70197e91a184083ea2ac9d4d75c47f0", null ],
    [ "mutt_hcache_fetch", "hcache_8c.html#a328c1cf43151360a9001b2423ffd6ab8", null ],
    [ "mutt_hcache_fetch_raw", "hcache_8c.html#ae65c9e1141287d9e21e541475df3f7bf", null ],
    [ "mutt_hcache_free", "hcache_8c.html#a51e16ac36abe59d7df4b6be113efe90c", null ],
    [ "mutt_hcache_store", "hcache_8c.html#a2506fd544924cac6eea25644d7057872", null ],
    [ "mutt_hcache_store_raw", "hcache_8c.html#add0651518cff8eb01d398664cd98037b", null ],
    [ "mutt_hcache_delete", "hcache_8c.html#af464c9289099045a67095846de0939de", null ],
    [ "mutt_hcache_backend_list", "hcache_8c.html#a19989208cbd44d4083d872702017e1fc", null ],
    [ "mutt_hcache_is_valid_backend", "hcache_8c.html#af2522d53121a164856680743be32ea18", null ],
    [ "HeaderCacheBackend", "hcache_8c.html#a741812afe210d1b16e38e53fe62bca16", null ],
    [ "hcachever", "hcache_8c.html#ac0dde9400cb5f702e4b8c6fbd6e5c0ab", null ],
    [ "hcache_ops", "hcache_8c.html#aafea30469d08ba46dc83b5abf0c88285", null ]
];