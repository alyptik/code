var structPopData =
[
    [ "conn", "structPopData.html#ad186a049452c398c82233ca7c73845d4", null ],
    [ "status", "structPopData.html#a27dc3e9f0c6d11f5c8e206026ebddb1c", null ],
    [ "capabilities", "structPopData.html#a9eb616084a87e1af277bca5988d0189d", null ],
    [ "use_stls", "structPopData.html#a7aea487217fccb3669583024c6e7e199", null ],
    [ "cmd_capa", "structPopData.html#ac665c4a8638244e7305014a65f17fcbb", null ],
    [ "cmd_stls", "structPopData.html#a8b14f30b488782aba98d4a20291cfe88", null ],
    [ "cmd_user", "structPopData.html#a476522562b22fb5b3fcb133adbf9e4f8", null ],
    [ "cmd_uidl", "structPopData.html#a07167d04c75738dab9d66cdf6e1ebd00", null ],
    [ "cmd_top", "structPopData.html#abaa94eba5f13078988168aa3a7a24b00", null ],
    [ "resp_codes", "structPopData.html#aa387de46a909cc134a70467461bd26a8", null ],
    [ "expire", "structPopData.html#aa1c6c98fb5db67e252e49bdac339c785", null ],
    [ "clear_cache", "structPopData.html#ad6e1ec1829094652a19107c409a2eec7", null ],
    [ "size", "structPopData.html#a92932c0f4eb8692359a794fe42af3556", null ],
    [ "check_time", "structPopData.html#a21811eb478ff9ac6784f0ff33f36f87a", null ],
    [ "login_delay", "structPopData.html#ac921723daace7a4ce20037dd088c8f83", null ],
    [ "auth_list", "structPopData.html#a5e64ea4b70e687e3a56fa74b30310f68", null ],
    [ "timestamp", "structPopData.html#aaace51467e2018daa5833a54d285aefb", null ],
    [ "bcache", "structPopData.html#a75f3f924b2cece843a202ae302f481fc", null ],
    [ "err_msg", "structPopData.html#aaa0f1a538d218f4b0a54c95b4d5c3328", null ],
    [ "cache", "structPopData.html#abbacec381240c583b52f671d59982c45", null ]
];