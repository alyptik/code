var structMailbox =
[
    [ "path", "structMailbox.html#ad9d3aa8c4dd96434ba5e07cc3986ee18", null ],
    [ "realpath", "structMailbox.html#a3539565f07c9dc3db0cd84706c8ff212", null ],
    [ "desc", "structMailbox.html#a567320abfd82896cb23d3f38a4331358", null ],
    [ "size", "structMailbox.html#ace276d581d6d299ecf227be214841148", null ],
    [ "new", "structMailbox.html#a53a475b638bbd24a12a6ad5618befc04", null ],
    [ "msg_count", "structMailbox.html#a3d7ee4badbaec0d67db9e29d5ba716ee", null ],
    [ "msg_unread", "structMailbox.html#af2632307a08699cf251e6599c8a84cac", null ],
    [ "msg_flagged", "structMailbox.html#ad8cc58166581060ab58acf36f22bfd4d", null ],
    [ "notified", "structMailbox.html#ac1915d0b0ff602ec1d219b979bfda59c", null ],
    [ "magic", "structMailbox.html#a950cb5b7a2ea907371d445b0a5b56582", null ],
    [ "newly_created", "structMailbox.html#a64e988bd2f8f1c9a27f4193523f4a336", null ],
    [ "last_visited", "structMailbox.html#a9200a45bfa78f50792135fe283cac477", null ],
    [ "stats_last_checked", "structMailbox.html#af611bf2fc9bda82c83889ba43c3ea8f5", null ]
];