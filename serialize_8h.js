var serialize_8h =
[
    [ "serial_dump_address", "serialize_8h.html#ad5055409a3bc0050687a1cb6cff1316d", null ],
    [ "serial_dump_body", "serialize_8h.html#a6da871504dc1ce006a1ff36c2183fd89", null ],
    [ "serial_dump_buffer", "serialize_8h.html#af15ef1293f730af186cd6a1797ae8bf4", null ],
    [ "serial_dump_char", "serialize_8h.html#a7fa6bf18ed1915cdfb49d945b6c018d3", null ],
    [ "serial_dump_char_size", "serialize_8h.html#a5de3417db2f58568d7da82b74571efd4", null ],
    [ "serial_dump_envelope", "serialize_8h.html#a3e2e3c391646ac618f7d55d21b6740f0", null ],
    [ "serial_dump_int", "serialize_8h.html#a4cb970e9ca646206b7428ae05d6fc62a", null ],
    [ "serial_dump_parameter", "serialize_8h.html#a2e94b9bab9c6e3d1e446b298fedd5886", null ],
    [ "serial_dump_stailq", "serialize_8h.html#aef94d95a80f0ecef941afc5b02913345", null ],
    [ "serial_restore_address", "serialize_8h.html#a3573897798dc0c5f8423073fb7f1d412", null ],
    [ "serial_restore_body", "serialize_8h.html#ad81529a7d4a01ecf7a6a289c625ffc36", null ],
    [ "serial_restore_buffer", "serialize_8h.html#a3ea7efb3dc11e1b1d07f528c9e759bbf", null ],
    [ "serial_restore_char", "serialize_8h.html#a43a280bdcd6ff4e026fbab74aa3f82c4", null ],
    [ "serial_restore_envelope", "serialize_8h.html#adf9a9b6ad3e890387bb7d793eef8d347", null ],
    [ "serial_restore_int", "serialize_8h.html#ae337dfc66e217ae0253a77eef4271f02", null ],
    [ "serial_restore_parameter", "serialize_8h.html#ae4444da9a7c0b6bf39522d28842bca3b", null ],
    [ "serial_restore_stailq", "serialize_8h.html#a7c71f04834a31c8f5d203016ef0e24ee", null ],
    [ "mutt_hcache_dump", "serialize_8h.html#aa5cdf3203623e58ef16a18ca9282743c", null ],
    [ "mutt_hcache_restore", "serialize_8h.html#a556940057108b6e02ff298c2aabe4e6a", null ]
];