var dir_803ea0153ad76bee9f0b07478cfbff67 =
[
    [ "account.h", "conn_2account_8h.html", [
      [ "Account", "structAccount.html", "structAccount" ]
    ] ],
    [ "conn.h", "conn_8h.html", "conn_8h" ],
    [ "conn_globals.c", "conn__globals_8c.html", "conn__globals_8c" ],
    [ "conn_globals.h", "conn__globals_8h.html", "conn__globals_8h" ],
    [ "conn_raw.c", "conn__raw_8c.html", "conn__raw_8c" ],
    [ "connection.h", "connection_8h.html", "connection_8h" ],
    [ "getdomain.c", "getdomain_8c.html", "getdomain_8c" ],
    [ "sasl.c", "sasl_8c.html", "sasl_8c" ],
    [ "sasl.h", "sasl_8h.html", "sasl_8h" ],
    [ "sasl_plain.c", "sasl__plain_8c.html", "sasl__plain_8c" ],
    [ "sasl_plain.h", "sasl__plain_8h.html", "sasl__plain_8h" ],
    [ "socket.c", "socket_8c.html", "socket_8c" ],
    [ "socket.h", "socket_8h.html", "socket_8h" ],
    [ "ssl.c", "ssl_8c.html", "ssl_8c" ],
    [ "ssl.h", "ssl_8h.html", "ssl_8h" ],
    [ "ssl_gnutls.c", "ssl__gnutls_8c.html", "ssl__gnutls_8c" ],
    [ "tunnel.c", "tunnel_8c.html", "tunnel_8c" ],
    [ "tunnel.h", "tunnel_8h.html", "tunnel_8h" ]
];