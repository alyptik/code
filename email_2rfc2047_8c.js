var email_2rfc2047_8c =
[
    [ "ENCWORD_LEN_MAX", "email_2rfc2047_8c.html#aed2999c145a004bc9c37fbd6248b822e", null ],
    [ "ENCWORD_LEN_MIN", "email_2rfc2047_8c.html#a72b4275b0634c8faba7296d1c1c9d97e", null ],
    [ "HSPACE", "email_2rfc2047_8c.html#ae5b0570d3494e680eada9f69039bdc87", null ],
    [ "CONTINUATION_BYTE", "email_2rfc2047_8c.html#acf2ed0701cb187050e2dae7c40d508a6", null ],
    [ "encoder_t", "email_2rfc2047_8c.html#a65c5603391bc8926b2cf079810945513", null ],
    [ "b_encoder", "email_2rfc2047_8c.html#a386450bad3229da26d1cd1a63d0fc257", null ],
    [ "q_encoder", "email_2rfc2047_8c.html#ac84a78b1788e7c21ab20df68a24c9cd0", null ],
    [ "parse_encoded_word", "email_2rfc2047_8c.html#a78942300d9cc890e47b32b24c5a4d2e1", null ],
    [ "try_block", "email_2rfc2047_8c.html#af517130be123255c119dae5b47f3cb1c", null ],
    [ "encode_block", "email_2rfc2047_8c.html#a8c4ea35c95b50455b895a18b7e4cbc10", null ],
    [ "choose_block", "email_2rfc2047_8c.html#a68424d046b66d891da1b80929074f616", null ],
    [ "finalize_chunk", "email_2rfc2047_8c.html#afef686b50d1bc6030d76a2c1c5dbb6c4", null ],
    [ "decode_word", "email_2rfc2047_8c.html#a7bc348d3286019d04ba25d666fb86901", null ],
    [ "encode", "email_2rfc2047_8c.html#ad304a6e40f646f43a411b649f960b306", null ],
    [ "rfc2047_encode", "email_2rfc2047_8c.html#a3cc43939d840544876d40c8da1f99b7d", null ],
    [ "rfc2047_decode", "email_2rfc2047_8c.html#a89c57c82465740567f73641c9397d760", null ],
    [ "rfc2047_encode_addrlist", "email_2rfc2047_8c.html#a19b03a80b48367cf63a33b4ce7f092ad", null ],
    [ "rfc2047_decode_addrlist", "email_2rfc2047_8c.html#a5b4551c4258a843325609099be84f39a", null ]
];