var group_8h =
[
    [ "Group", "structGroup.html", "structGroup" ],
    [ "GroupContext", "structGroupContext.html", "structGroupContext" ],
    [ "MUTT_GROUP", "group_8h.html#a1c6dfb6c372cb2f37b9d2f5573580c83", null ],
    [ "MUTT_UNGROUP", "group_8h.html#abfd4ec99911433b6d88c6eed9ef61e26", null ],
    [ "mutt_group_context_add", "group_8h.html#a9a6795c02f6aaaec4d026a40878398a1", null ],
    [ "mutt_group_context_destroy", "group_8h.html#ab72e9a6349098240dfa2257789226cc4", null ],
    [ "mutt_group_context_add_addrlist", "group_8h.html#ab532b0aac627bf0b1536a6868e1ab471", null ],
    [ "mutt_group_context_add_regex", "group_8h.html#ab13fe1dc2aabed615c1e1770626f283a", null ],
    [ "mutt_group_match", "group_8h.html#a85d4636eab11d9c91cc6af8840819f86", null ],
    [ "mutt_group_context_clear", "group_8h.html#a8660c3d7becb5bef2c3725019e4f9950", null ],
    [ "mutt_group_context_remove_regex", "group_8h.html#afd7a739e11c90470771dabe35e4adad7", null ],
    [ "mutt_group_context_remove_addrlist", "group_8h.html#acc0f422c0d51c27ecd4e34e2aa2c2aec", null ],
    [ "mutt_pattern_group", "group_8h.html#a155eff28c896fa67f4c316902c2a3fb9", null ]
];