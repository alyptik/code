var searchData=
[
  ['x_5fcomment_5fto',['x_comment_to',['../structEnvelope.html#a52153c6e4fdb551233e26d4fdb48fb78',1,'Envelope']]],
  ['x_5flabel',['x_label',['../structEnvelope.html#ac7bed8a507227756030c035a7aad198e',1,'Envelope']]],
  ['x_5foriginal_5fto',['x_original_to',['../structEnvelope.html#ab6706015f7d6406a12d41b0a2b165dd4',1,'Envelope']]],
  ['xcommentto',['XCommentTo',['../globals_8h.html#aefe08ed8b9a8490c20d414f26106339b',1,'globals.h']]],
  ['xcred',['xcred',['../structTlsSockData.html#a44b81a79c0f6cd2185054a494b9eb193',1,'TlsSockData']]],
  ['xdg_5fdefaults',['xdg_defaults',['../muttlib_8c.html#acc0475757fa09772b783f791f2c41a80',1,'muttlib.c']]],
  ['xdg_5fenv_5fvars',['xdg_env_vars',['../muttlib_8c.html#aa56f51d9542560ead857cd669e7081fe',1,'muttlib.c']]],
  ['xlabel_5fchanged',['xlabel_changed',['../structHeader.html#ab16b7fa20d0a0fc242a24870f673c209',1,'Header']]],
  ['xref',['xref',['../structEnvelope.html#aafcabc33a66b5edfa91bbdc922515c2e',1,'Envelope']]],
  ['xtype',['xtype',['../structBody.html#a25c15115b34c11620fc995c13383fd9b',1,'Body']]]
];
