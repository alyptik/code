var searchData=
[
  ['register_20crypto_20modules',['Register crypto modules',['../crypt_crypt_mod.html',1,'ncrypt']]],
  ['representation_20of_20an_20email_20address',['Representation of an email address',['../email_address.html',1,'email']]],
  ['representation_20of_20the_20body_20of_20an_20email',['Representation of the body of an email',['../email_body.html',1,'email']]],
  ['representation_20of_20an_20email_20header_20_28envelope_29',['Representation of an email header (envelope)',['../email_envelope.html',1,'email']]],
  ['representation_20of_20the_20email_27s_20header',['Representation of the email&apos;s header',['../email_header.html',1,'email']]],
  ['rfc2047_20encoding_20_2f_20decoding_20functions',['RFC2047 encoding / decoding functions',['../email_rfc2047.html',1,'email']]],
  ['rfc2231_20mime_20charset_20routines',['RFC2231 MIME Charset routines',['../email_rfc2231.html',1,'email']]],
  ['read_2fwrite_20command_20history_20from_2fto_20a_20file',['Read/write command history from/to a file',['../history.html',1,'mutt']]],
  ['read_2fparse_2fwrite_20an_20nntp_20config_20file_20of_20subscribed_20newsgroups',['Read/parse/write an NNTP config file of subscribed newsgroups',['../nntp_newsrc.html',1,'nntp']]],
  ['rfc1524_20mailcap_20routines',['RFC1524 Mailcap routines',['../rfc1524.html',1,'index']]]
];
