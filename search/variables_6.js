var searchData=
[
  ['g',['g',['../structGroupContext.html#a7289ada3fdfe78dd637e8fd84d1c3686',1,'GroupContext::g()'],['../structPattern.html#a7a5e4259a0107341f27f1f5aafc61b69',1,'Pattern::g()']]],
  ['gecosmask',['GecosMask',['../muttlib_8c.html#aefda0782d350131552437816cdd65512',1,'GecosMask():&#160;muttlib.c'],['../muttlib_8h.html#aefda0782d350131552437816cdd65512',1,'GecosMask():&#160;muttlib.c']]],
  ['gen_5fhash',['gen_hash',['../structHash.html#a780bf362439c87059e0e963052f2b008',1,'Hash']]],
  ['gen_5ftime',['gen_time',['../structPgpKeyInfo.html#a75b7dba5f4f4409f023b0f51890da4dd',1,'PgpKeyInfo']]],
  ['gid',['gid',['../structFolderFile.html#a5a5786506959747b14573f280a8e5a7c',1,'FolderFile']]],
  ['gitver',['GitVer',['../globals_8h.html#adc526a91dddf9717c5e96ef6e85d19d7',1,'globals.h']]],
  ['goodsig',['goodsig',['../structBody.html#ae4b2706a5c4601ae1497a96e047d8f5e',1,'Body']]],
  ['group',['group',['../structAddress.html#a12bd44ccc016e50c674ec3a902ba1c28',1,'Address::group()'],['../structNntpData.html#a6283880e7891eb089f871e92b9e9a6d0',1,'NntpData::group()']]],
  ['groupindexformat',['GroupIndexFormat',['../browser_8c.html#ac9a2c4763aa721e78d4a34a871cbeb2a',1,'GroupIndexFormat():&#160;browser.c'],['../browser_8h.html#ac9a2c4763aa721e78d4a34a871cbeb2a',1,'GroupIndexFormat():&#160;browser.c']]],
  ['groupmatch',['groupmatch',['../structPattern.html#af1fa8219f7dc92e209b45c2b3e43c717',1,'Pattern']]],
  ['groups',['Groups',['../globals_8h.html#a52768eed10605c9d6db04c9f18d709ce',1,'globals.h']]],
  ['groups_5fhash',['groups_hash',['../structNntpServer.html#a8f2f0373552758a858779e12fa52dbdc',1,'NntpServer']]],
  ['groups_5flist',['groups_list',['../structNntpServer.html#aed9ac2ebc6cec5d5b1f79efa7a47be59',1,'NntpServer']]],
  ['groups_5fmax',['groups_max',['../structNntpServer.html#a92c1d5731fb6e8fe1ef757e325dd7508',1,'NntpServer']]],
  ['groups_5fnum',['groups_num',['../structNntpServer.html#acb18db3314c3562e6fea6e28894cf6f7',1,'NntpServer']]]
];
