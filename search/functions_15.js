var searchData=
[
  ['valid_5fpgp_5fencrypted_5fhandler',['valid_pgp_encrypted_handler',['../handler_8c.html#ab28bc87c5043dbacd5d51df806332abe',1,'handler.c']]],
  ['valid_5fsmtp_5fcode',['valid_smtp_code',['../smtp_8c.html#a776998bed1d28b6fc2ba33f7fa183644',1,'smtp.c']]],
  ['validator_5ffail',['validator_fail',['../common_8c.html#a62b0ccb8d82b0fc9d23f38ba66c6a10e',1,'validator_fail(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c'],['../common_8h.html#a62b0ccb8d82b0fc9d23f38ba66c6a10e',1,'validator_fail(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c']]],
  ['validator_5fsucceed',['validator_succeed',['../common_8c.html#ac552ac4c600abc7ccd1894af2cbdd9c6',1,'validator_succeed(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c'],['../common_8h.html#ac552ac4c600abc7ccd1894af2cbdd9c6',1,'validator_succeed(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c']]],
  ['validator_5fwarn',['validator_warn',['../common_8c.html#a01895b76131f2b177dc31f2bc56fa690',1,'validator_warn(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c'],['../common_8h.html#a01895b76131f2b177dc31f2bc56fa690',1,'validator_warn(const struct ConfigSet *cs, const struct ConfigDef *cdef, intptr_t value, struct Buffer *result):&#160;common.c']]],
  ['var_5fto_5fstring2',['var_to_string2',['../init_8c.html#ad36e6b14c1df71ea7dd5ecd74be8dace',1,'var_to_string2(const char *name, bool quote, struct Buffer *result, struct Buffer *err):&#160;init.c'],['../myvar_8h.html#ad36e6b14c1df71ea7dd5ecd74be8dace',1,'var_to_string2(const char *name, bool quote, struct Buffer *result, struct Buffer *err):&#160;init.c']]],
  ['verify_5fkey',['verify_key',['../crypt__gpgme_8c.html#abf137f94451de251c8179788615f4b74',1,'crypt_gpgme.c']]],
  ['verify_5fone',['verify_one',['../crypt__gpgme_8c.html#a490d3344cd29fb3466bc7868e4b26c87',1,'crypt_gpgme.c']]],
  ['verify_5fsender',['verify_sender',['../crypt__gpgme_8c.html#a6dbc903ec828a16285bdd303b8214983',1,'crypt_gpgme.c']]],
  ['vfolder_5fentry',['vfolder_entry',['../browser_8c.html#a815ed37e2a0b581a082ae19b16a95726',1,'browser.c']]]
];
