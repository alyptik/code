var searchData=
[
  ['string_20auto_2dcompletion_20routines',['String auto-completion routines',['../complete.html',1,'index']]],
  ['sasl_20authentication_20support',['SASL authentication support',['../conn_sasl.html',1,'conn']]],
  ['sasl_20plain_20authentication_20support',['SASL plain authentication support',['../conn_sasl_plain.html',1,'conn']]],
  ['support_20for_20network_20tunnelling',['Support for network tunnelling',['../conn_tunnel.html',1,'conn']]],
  ['signing_2fencryption_20multiplexor',['Signing/encryption multiplexor',['../crypt_crypt.html',1,'ncrypt']]],
  ['smime_20helper_20routines',['SMIME helper routines',['../crypt_smime.html',1,'ncrypt']]],
  ['store_20attributes_20associated_20with_20a_20mime_20part',['Store attributes associated with a MIME part',['../email_parameter.html',1,'email']]],
  ['send_2freceive_20commands_20to_2ffrom_20an_20imap_20server',['Send/receive commands to/from an IMAP server',['../imap_command.html',1,'imap']]],
  ['singly_2dlinked_20list_20type',['Singly-linked list type',['../list.html',1,'mutt']]],
  ['signal_20handling',['Signal handling',['../mutt_signal.html',1,'index']]],
  ['save_2frestore_20and_20gui_20list_20postponed_20emails',['Save/restore and GUI list postponed emails',['../postpone.html',1,'index']]],
  ['signal_20handling',['Signal handling',['../signal.html',1,'mutt']]],
  ['string_20manipulation_20functions',['String manipulation functions',['../string.html',1,'mutt']]],
  ['set_20the_20terminal_20title_2ficon',['Set the terminal title/icon',['../terminal.html',1,'index']]]
];
