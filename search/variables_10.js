var searchData=
[
  ['r',['r',['../structCoord.html#ae707cab2f8853900fa3387130717eb9a',1,'Coord']]],
  ['range_5fregexes',['range_regexes',['../pattern_8c.html#ae874bbe6a505a361ef27925d0e6e8f24',1,'pattern.c']]],
  ['raw',['raw',['../structRangeRegex.html#af7bb292bc1d13955aae8df3cbf40c652',1,'RangeRegex']]],
  ['reachingus',['ReachingUs',['../version_8c.html#a440c5f4913af8663a4d6c5626b44c11d',1,'version.c']]],
  ['read',['read',['../structHeader.html#a061a4c67e4eac143e70a9bb8e472cae9',1,'Header::read()'],['../structImapHeaderData.html#a364cc396517521c52f593968529729ce',1,'ImapHeaderData::read()'],['../structMessage.html#ace4606e8dc8aa0c35738e9f788f47705',1,'Message::read()']]],
  ['readfd',['readfd',['../structTunnelData.html#abe8554665a026f451164b6817687b689',1,'TunnelData']]],
  ['readinc',['ReadInc',['../globals_8h.html#a9ac13446d164d9fb95f8530395e41794',1,'globals.h']]],
  ['readonly',['readonly',['../structContext.html#a9a0201ffbf48d007b2b6c268ad7f4467',1,'Context::readonly()'],['../globals_8h.html#a742f5471fe4275a2cd6793848c302c8f',1,'ReadOnly():&#160;globals.h']]],
  ['ready',['ready',['../structRangeRegex.html#a68dfa785ce133eb54192515277fe9913',1,'RangeRegex']]],
  ['real_5fsubj',['real_subj',['../structEnvelope.html#adea069c71a9e00c79e1233f0fb51f824',1,'Envelope']]],
  ['realname',['Realname',['../globals_8h.html#a502b24edd599af391539d8d535c5b445',1,'globals.h']]],
  ['realpath',['realpath',['../structContext.html#a1595f4d8121f11827e8b026e66d28bb1',1,'Context::realpath()'],['../structMailbox.html#a3539565f07c9dc3db0cd84706c8ff212',1,'Mailbox::realpath()']]],
  ['recall',['Recall',['../send_8c.html#ab3ddd30a2bb5b715f4e0298c02d7e96f',1,'Recall():&#160;send.c'],['../send_8h.html#ab3ddd30a2bb5b715f4e0298c02d7e96f',1,'Recall():&#160;send.c']]],
  ['received',['received',['../structHeader.html#ad3ebade134f498c980691a603cc252ec',1,'Header::received()'],['../structImapHeader.html#a18937a689b628b43ac8d7718eb951544',1,'ImapHeader::received()'],['../structMessage.html#aa586337aebc4a460be8ae0f676cd0ff6',1,'Message::received()']]],
  ['recent',['recent',['../structImapStatus.html#a1b5112800d42cb86271a11679eac05c8',1,'ImapStatus']]],
  ['recip_5fvalid',['recip_valid',['../structHeader.html#a488492399d7a4ccb0651d2da01436dcd',1,'Header']]],
  ['recipient',['recipient',['../structHeader.html#ac87f9a1cf93e69863b5e9138d3aacac0',1,'Header']]],
  ['record',['Record',['../globals_8h.html#a2284d2d11a4002904fe297eea5ad9fde',1,'globals.h']]],
  ['recovering',['recovering',['../structImapData.html#a97c7f64eb6ad68302ed64513249eee6d',1,'ImapData']]],
  ['redraw',['redraw',['../structMenu.html#ae986265421ab66040ea408b44db361b0',1,'Menu']]],
  ['redraw_5fdata',['redraw_data',['../structMenu.html#aee28479c5794b00ca361cfbe872516b0',1,'Menu']]],
  ['references',['references',['../structEnvelope.html#a832eec73356a806761e26f0bc40d9083',1,'Envelope']]],
  ['reflowspacequotes',['ReflowSpaceQuotes',['../rfc3676_8c.html#a900be8f0767121fca971e5b6531df09d',1,'ReflowSpaceQuotes():&#160;rfc3676.c'],['../rfc3676_8h.html#a900be8f0767121fca971e5b6531df09d',1,'ReflowSpaceQuotes():&#160;rfc3676.c']]],
  ['reflowtext',['ReflowText',['../handler_8c.html#a0ba55529962c5d4f67eba68fe28b2fc7',1,'ReflowText():&#160;handler.c'],['../handler_8h.html#a0ba55529962c5d4f67eba68fe28b2fc7',1,'ReflowText():&#160;handler.c']]],
  ['reflowwrap',['ReflowWrap',['../rfc3676_8c.html#ae875a7693212a2b3514532a5fd4c71fb',1,'ReflowWrap():&#160;rfc3676.c'],['../rfc3676_8h.html#ae875a7693212a2b3514532a5fd4c71fb',1,'ReflowWrap():&#160;rfc3676.c']]],
  ['refno',['refno',['../structHeader.html#a396e11881576d9c195eabbb1e291511e',1,'Header']]],
  ['refs_5fchanged',['refs_changed',['../structEnvelope.html#ae3d56ec04b362af5854fe25ba3384944',1,'Envelope']]],
  ['regex',['regex',['../structHook.html#a1c6336f4645125bf795ad741e925bd86',1,'Hook::regex()'],['../structLookup.html#ab8893578bc138302e089ee40fea03fee',1,'Lookup::regex()'],['../structRegex.html#af9ccc25da6324c9055e53966abe9a7b3',1,'Regex::regex()'],['../structRegexList.html#a4bd746f0e1a190850d54fa270108c41b',1,'RegexList::regex()'],['../structReplaceList.html#a94e7fe211d3338df519bd6abb5c41779',1,'ReplaceList::regex()'],['../structColorLine.html#aa46967e3d6b7b0820a03b7f167591055',1,'ColorLine::regex()'],['../structPattern.html#a31a5db2d7fccfc8eccb942932b4937b4',1,'Pattern::regex()']]],
  ['registeredtypes',['RegisteredTypes',['../config_2set_8c.html#a2deafcb135c925caecec974914365475',1,'set.c']]],
  ['remailerhelp',['RemailerHelp',['../remailer_8c.html#add993060873690b1eea4383e8356a48a',1,'remailer.c']]],
  ['reopen',['reopen',['../structImapData.html#af93ae8256daae8935ce702b373b871c5',1,'ImapData']]],
  ['replacement',['replacement',['../structLookup.html#a25a23240d9698ea3e760736f1e516c3c',1,'Lookup']]],
  ['replacementchar',['ReplacementChar',['../charset_8c.html#ac31e11b5fb12abda40361188af9105ef',1,'ReplacementChar():&#160;charset.c'],['../charset_8h.html#ac31e11b5fb12abda40361188af9105ef',1,'ReplacementChar():&#160;charset.c']]],
  ['replied',['replied',['../structHeader.html#ada075066db01d4d555d629bcf6766455',1,'Header::replied()'],['../structImapHeaderData.html#a0ec82ceed8d3357c9f07ae42c1d7b4b6',1,'ImapHeaderData::replied()'],['../structMessage.html#a7fb38cfeca11b8aee9cd598475100dfb',1,'Message::replied()']]],
  ['reply_5fto',['reply_to',['../structEnvelope.html#a501f1a2dd49329ce100828c8c79025ba',1,'Envelope']]],
  ['replyregex',['ReplyRegex',['../email__globals_8c.html#a5383929841dd91e5476c8a2866e31050',1,'ReplyRegex():&#160;email_globals.c'],['../email__globals_8h.html#a5383929841dd91e5476c8a2866e31050',1,'ReplyRegex():&#160;email_globals.c']]],
  ['replyself',['ReplySelf',['../send_8c.html#aabff870432738b0debbb68b432b64ecd',1,'ReplySelf():&#160;send.c'],['../send_8h.html#aabff870432738b0debbb68b432b64ecd',1,'ReplySelf():&#160;send.c']]],
  ['replyto',['ReplyTo',['../send_8c.html#a5c4db7ad549922a43d6a0ca64acb4c3d',1,'ReplyTo():&#160;send.c'],['../send_8h.html#a5c4db7ad549922a43d6a0ca64acb4c3d',1,'ReplyTo():&#160;send.c']]],
  ['replywithxorig',['ReplyWithXorig',['../send_8c.html#a60d7228b6980ca86fd597b8aef6b2d68',1,'ReplyWithXorig():&#160;send.c'],['../send_8h.html#a60d7228b6980ca86fd597b8aef6b2d68',1,'ReplyWithXorig():&#160;send.c']]],
  ['reset',['reset',['../structConfigSetType.html#a253b34f6f11509bb2aca8e3185a45191',1,'ConfigSetType']]],
  ['resize',['Resize',['../pager_8c.html#a9129741ec9453d53665f9f00dd13c916',1,'pager.c']]],
  ['resolve',['Resolve',['../globals_8h.html#a01e26e329597fa3632f4ac781310503a',1,'globals.h']]],
  ['resp_5fcodes',['resp_codes',['../structPopData.html#aa387de46a909cc134a70467461bd26a8',1,'PopData']]],
  ['restore',['restore',['../structFetchCtx.html#a3a11993b69a8c8020c7c6b19b59f4121',1,'FetchCtx']]],
  ['resumedraftfiles',['ResumeDraftFiles',['../globals_8h.html#a90717f6c884c9dc0163c1414a94bfe0d',1,'globals.h']]],
  ['resumeediteddraftfiles',['ResumeEditedDraftFiles',['../main_8c.html#af6a2aeb7d2792e2b556ce925c5e0e55c',1,'ResumeEditedDraftFiles():&#160;main.c'],['../main_8h.html#af6a2aeb7d2792e2b556ce925c5e0e55c',1,'ResumeEditedDraftFiles():&#160;main.c']]],
  ['return_5fpath',['return_path',['../structEnvelope.html#a495a58c83e7384e13d5fb66b2338c67b',1,'Envelope']]],
  ['reversealias',['ReverseAlias',['../sort_8c.html#abdff29c6023f9dd2fb93d2889391ac6e',1,'ReverseAlias():&#160;sort.c'],['../sort_8h.html#abdff29c6023f9dd2fb93d2889391ac6e',1,'ReverseAlias():&#160;sort.c']]],
  ['reversealiases',['ReverseAliases',['../globals_8h.html#afeaf13c0b7691eab06238817e782fe5b',1,'globals.h']]],
  ['reversename',['ReverseName',['../send_8c.html#a7f8b307454612856239b0a7f1bd21e79',1,'ReverseName():&#160;send.c'],['../send_8h.html#a7f8b307454612856239b0a7f1bd21e79',1,'ReverseName():&#160;send.c']]],
  ['reverserealname',['ReverseRealname',['../send_8c.html#a67f7490cd74b54e7a3a7dc3b9dc5db0e',1,'ReverseRealname():&#160;send.c'],['../send_8h.html#a67f7490cd74b54e7a3a7dc3b9dc5db0e',1,'ReverseRealname():&#160;send.c']]],
  ['rfc2047parameters',['Rfc2047Parameters',['../rfc2231_8c.html#a9ebabe1af1300630d8d0c7f17ef8fe12',1,'Rfc2047Parameters():&#160;rfc2231.c'],['../rfc2231_8h.html#a9ebabe1af1300630d8d0c7f17ef8fe12',1,'Rfc2047Parameters():&#160;rfc2231.c']]],
  ['rgrp',['rgrp',['../structRangeRegex.html#ac4e25ea4b622354a12208d2e8e0375b6',1,'RangeRegex']]],
  ['rights',['rights',['../structContext.html#a779e3d476c66dd99be15e588c24808b1',1,'Context']]],
  ['root_5ffp',['root_fp',['../structAttachCtx.html#a3cded0aa389f6219df3e80204ebe8ea8',1,'AttachCtx']]],
  ['row_5foffset',['row_offset',['../structMuttWindow.html#aa2adfd03e0827261343224a7d886c282',1,'MuttWindow']]],
  ['rows',['rows',['../structMuttWindow.html#a3f728909f27b71071be267222f5a5db3',1,'MuttWindow']]],
  ['rs',['rs',['../structGroup.html#aca06d89102aac7513cb582d360fa00cd',1,'Group']]],
  ['rx_5fspecial_5fchars',['rx_special_chars',['../file_8c.html#a2f2b5de442494ad10eab11a78d79959c',1,'file.c']]]
];
