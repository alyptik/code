var searchData=
[
  ['sasl_5fir',['SASL_IR',['../imap__private_8h.html#a66a6fdb5d1044adb27a035c818994917afef15fa37a75d4da2bef28b4698414b9',1,'imap_private.h']]],
  ['sb_5fdiv_5fascii',['SB_DIV_ASCII',['../sidebar_8c.html#a1df81cd29a17636099f97add487c0b7dac250c60ac8b34ef8bcc751c44138fc0f',1,'sidebar.c']]],
  ['sb_5fdiv_5fuser',['SB_DIV_USER',['../sidebar_8c.html#a1df81cd29a17636099f97add487c0b7dae4e23eeb1efcfe8750f5313ca54f1ae4',1,'sidebar.c']]],
  ['sb_5fdiv_5futf8',['SB_DIV_UTF8',['../sidebar_8c.html#a1df81cd29a17636099f97add487c0b7dafa4164d5d9eb4d4b11d5d9b2a27da742',1,'sidebar.c']]],
  ['sb_5fsrc_5fincoming',['SB_SRC_INCOMING',['../sidebar_8c.html#ab4009fad9cbd56c4d989dc912cf4e75ba3de4c70d6fad25b2147a41216bc94af5',1,'sidebar.c']]],
  ['sb_5fsrc_5fvirt',['SB_SRC_VIRT',['../sidebar_8c.html#ab4009fad9cbd56c4d989dc912cf4e75ba152cc8716a88356fffab3f6b05706a6d',1,'sidebar.c']]],
  ['smtputf8',['SMTPUTF8',['../smtp_8c.html#a9edc7e7a37466251223bc0e03443814ba11ab51c9490b3f390e650b8826dc1fe4',1,'smtp.c']]],
  ['starttls',['STARTTLS',['../imap__private_8h.html#a66a6fdb5d1044adb27a035c818994917ae7cc0b2670caf4784fa8209e95971757',1,'STARTTLS():&#160;imap_private.h'],['../smtp_8c.html#a9edc7e7a37466251223bc0e03443814bae7cc0b2670caf4784fa8209e95971757',1,'STARTTLS():&#160;smtp.c']]],
  ['status',['STATUS',['../imap__private_8h.html#a66a6fdb5d1044adb27a035c818994917ab15379688176677d49474245a6178d97',1,'imap_private.h']]]
];
