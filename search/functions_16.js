var searchData=
[
  ['wcscasecmp',['wcscasecmp',['../protos_8h.html#aece8f8a0bf2359e8f8cfc6b7c9f6303f',1,'wcscasecmp(const wchar_t *a, const wchar_t *b):&#160;wcscasecmp.c'],['../wcscasecmp_8c.html#aece8f8a0bf2359e8f8cfc6b7c9f6303f',1,'wcscasecmp(const wchar_t *a, const wchar_t *b):&#160;wcscasecmp.c']]],
  ['windowed_5fquery_5ffrom_5fquery',['windowed_query_from_query',['../mutt__notmuch_8c.html#a3c1e6b40d6cc7a396ef95d90e4364337',1,'mutt_notmuch.c']]],
  ['write_5fas_5ftext_5fpart',['write_as_text_part',['../sendlib_8c.html#a55c55e2f50317ab8226c0c2768e01ad0',1,'sendlib.c']]],
  ['write_5fone_5fheader',['write_one_header',['../sendlib_8c.html#aa0136a8fef4cce085d3dcc5baf6a343a',1,'sendlib.c']]],
  ['write_5fsafe_5faddress',['write_safe_address',['../alias_8c.html#a72f25c630f91fffd47d013745fae3b74',1,'alias.c']]]
];
