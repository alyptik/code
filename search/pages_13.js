var searchData=
[
  ['wrapper_20for_20pgp_2fsmime_20calls_20to_20gpgme',['Wrapper for PGP/SMIME calls to GPGME',['../crypt_crypt_gpgme.html',1,'ncrypt']]],
  ['wrappers_20for_20calls_20to_20cli_20pgp',['Wrappers for calls to CLI PGP',['../crypt_crypt_mod_pgp.html',1,'ncrypt']]],
  ['wrappers_20for_20calls_20to_20gpgme_20pgp',['Wrappers for calls to GPGME PGP',['../crypt_crypt_mod_pgp_gpgme.html',1,'ncrypt']]],
  ['wrappers_20for_20calls_20to_20cli_20smime',['Wrappers for calls to CLI SMIME',['../crypt_crypt_mod_smime.html',1,'ncrypt']]],
  ['wrappers_20for_20calls_20to_20gpgme_20smime',['Wrappers for calls to GPGME SMIME',['../crypt_crypt_mod_smime_gpgme.html',1,'ncrypt']]],
  ['wrapper_20around_20crypto_20functions',['Wrapper around crypto functions',['../crypt_cryptglue.html',1,'ncrypt']]],
  ['wrapper_20around_20calls_20to_20external_20pgp_20program',['Wrapper around calls to external PGP program',['../crypt_pgpinvoke.html',1,'ncrypt']]],
  ['wrapper_20for_20vasprintf_28_29',['Wrapper for vasprintf()',['../safe_asprintf.html',1,'index']]],
  ['window_20management',['Window management',['../window.html',1,'index']]]
];
