var searchData=
[
  ['general_20purpose_20object_20for_20storing_20and_20parsing_20strings',['General purpose object for storing and parsing strings',['../buffer.html',1,'mutt']]],
  ['gui_20ask_20the_20user_20to_20enter_20a_20string',['GUI ask the user to enter a string',['../enter.html',1,'index']]],
  ['gdmb',['GDMB',['../hc_gdbm.html',1,'hcache']]],
  ['gui_20handle_20the_20resizing_20of_20the_20screen',['GUI handle the resizing of the screen',['../resize.html',1,'index']]],
  ['gui_20display_20the_20mailboxes_20in_20a_20side_20panel',['GUI display the mailboxes in a side panel',['../sidebar.html',1,'index']]],
  ['gui_20display_20a_20user_2dconfigurable_20status_20line',['GUI display a user-configurable status line',['../status.html',1,'index']]]
];
