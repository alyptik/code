var searchData=
[
  ['handling_20of_20openssl_20encryption',['Handling of OpenSSL encryption',['../conn_ssl.html',1,'conn']]],
  ['handling_20of_20gnutls_20encryption',['Handling of GnuTLS encryption',['../conn_ssl_gnutls.html',1,'conn']]],
  ['handling_20of_20email_20attachments',['Handling of email attachments',['../email_attach.html',1,'email']]],
  ['handling_20of_20international_20domain_20names',['Handling of international domain names',['../email_idna.html',1,'email']]],
  ['hash_20table_20data_20structure',['Hash table data structure',['../hash.html',1,'mutt']]],
  ['header_20cache_20multiplexor',['Header cache multiplexor',['../hc_hcache.html',1,'hcache']]],
  ['hcache_3a_20header_20cache_20api',['HCACHE: Header cache API',['../hcache.html',1,'']]]
];
