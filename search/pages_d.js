var searchData=
[
  ['parse_20the_20output_20of_20cli_20pgp_20program',['Parse the output of CLI PGP program',['../crypt_gnupg.html',1,'ncrypt']]],
  ['pgp_20sign_2c_20encrypt_2c_20check_20routines',['PGP sign, encrypt, check routines',['../crypt_pgp.html',1,'ncrypt']]],
  ['pgp_20key_20management_20routines',['PGP key management routines',['../crypt_pgpkey.html',1,'ncrypt']]],
  ['parse_20pgp_20data_20packets',['Parse PGP data packets',['../crypt_pgppacket.html',1,'ncrypt']]],
  ['prepare_20an_20email_20to_20be_20edited',['Prepare an email to be edited',['../editmsg.html',1,'index']]],
  ['parse_20and_20identify_20different_20url_20schemes',['Parse and identify different URL schemes',['../email_url.html',1,'email']]],
  ['private_20copy_20of_20the_20environment_20variables',['Private copy of the environment variables',['../envlist.html',1,'mutt']]],
  ['pass_20files_20through_20external_20commands_20_28filters_29',['Pass files through external commands (filters)',['../filter.html',1,'index']]],
  ['parse_20and_20execute_20user_2ddefined_20hooks',['Parse and execute user-defined hooks',['../hook.html',1,'index']]],
  ['path_20manipulation_20functions',['Path manipulation functions',['../path.html',1,'mutt']]],
  ['pop_3a_20network_20mailbox',['POP: Network mailbox',['../pop.html',1,'']]],
  ['pop_20authentication',['POP authentication',['../pop_auth.html',1,'pop']]],
  ['pop_20helper_20routines',['POP helper routines',['../pop_lib.html',1,'pop']]],
  ['pop_20network_20mailbox',['POP network mailbox',['../pop_pop.html',1,'pop']]],
  ['progress_20bar',['Progress bar',['../progress.html',1,'index']]]
];
