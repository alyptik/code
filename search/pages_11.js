var searchData=
[
  ['type_3a_20email_20address',['Type: Email address',['../config-address.html',1,'config']]],
  ['type_3a_20boolean',['Type: Boolean',['../config-bool.html',1,'config']]],
  ['type_3a_20command',['Type: Command',['../config-command.html',1,'config']]],
  ['type_3a_20long',['Type: Long',['../config-long.html',1,'config']]],
  ['type_3a_20mailbox_20types',['Type: Mailbox types',['../config-magic.html',1,'config']]],
  ['type_3a_20multi_2dbyte_20character_20table',['Type: Multi-byte character table',['../config-mbtable.html',1,'config']]],
  ['type_3a_20number',['Type: Number',['../config-number.html',1,'config']]],
  ['type_3a_20path',['Type: Path',['../config-path.html',1,'config']]],
  ['type_3a_20quad_2doption',['Type: Quad-option',['../config-quad.html',1,'config']]],
  ['type_3a_20regular_20expression',['Type: Regular expression',['../config-regex.html',1,'config']]],
  ['type_3a_20sorting',['Type: Sorting',['../config-sort.html',1,'config']]],
  ['type_3a_20string',['Type: String',['../config-string.html',1,'config']]],
  ['time_20and_20date_20handling_20routines',['Time and date handling routines',['../date.html',1,'mutt']]],
  ['tokyo_20cabinet',['Tokyo Cabinet',['../hc_tc.html',1,'hcache']]]
];
