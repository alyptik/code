var structNmCtxData =
[
    [ "db", "structNmCtxData.html#a11ca18e65c83996d91ee08b4a4a4b36a", null ],
    [ "db_url", "structNmCtxData.html#a1992399a482ef76c0c8930f463ed426f", null ],
    [ "db_url_holder", "structNmCtxData.html#ad3b5a26a8a79fd52586a4203ebfac907", null ],
    [ "db_query", "structNmCtxData.html#a81acc976383ebaa8c43b48101942f777", null ],
    [ "db_limit", "structNmCtxData.html#a341a24bafd3cb0d0e1b773fc60843d73", null ],
    [ "query_type", "structNmCtxData.html#a94e21dac07c40e6caa7157a29a689472", null ],
    [ "progress", "structNmCtxData.html#adc14492175f0754f8c32c0e0bf352c24", null ],
    [ "oldmsgcount", "structNmCtxData.html#a6fe06dcf1dd9cfb3554b2d7aa3066596", null ],
    [ "ignmsgcount", "structNmCtxData.html#a5c9569a9fa6d901248312854d73eef0e", null ],
    [ "noprogress", "structNmCtxData.html#a203dd0960fac717644e065125575d4da", null ],
    [ "longrun", "structNmCtxData.html#a063fa17c3d395a200eb889077f3d9900", null ],
    [ "trans", "structNmCtxData.html#a7229e76a88131f1444b67c4d62781557", null ],
    [ "progress_ready", "structNmCtxData.html#aa397bf6c7f45e5a06b9cdde88a279bc2", null ]
];