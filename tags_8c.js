var tags_8c =
[
    [ "driver_tags_getter", "tags_8c.html#a64e445048fcbb1b0058e80f24de6056f", null ],
    [ "driver_tags_add", "tags_8c.html#adf04cca115af041895d535fbf3900a33", null ],
    [ "driver_tags_free", "tags_8c.html#a825bfe477d7a38a6619cdb4b0bbd93c8", null ],
    [ "driver_tags_get_transformed", "tags_8c.html#ab03a878895c01509e047710046fc2af3", null ],
    [ "driver_tags_get", "tags_8c.html#ace0ffc57812ab1fd3ba00a2cc780653b", null ],
    [ "driver_tags_get_with_hidden", "tags_8c.html#a344c0367ea282a452ca71a8ca7bef6ea", null ],
    [ "driver_tags_get_transformed_for", "tags_8c.html#a927f9363266043c2105c5695dfaba0af", null ],
    [ "driver_tags_replace", "tags_8c.html#ae366e8246a8491af8abed2544643e54b", null ],
    [ "HiddenTags", "tags_8c.html#ab10d0d9c8c432c370b8759b4b1d7e423", null ],
    [ "TagTransforms", "tags_8c.html#a78519b9c49a46ce5423e470190b1aee3", null ]
];