var test_2config_2address_8c =
[
    [ "TEST_NO_MAIN", "test_2config_2address_8c.html#a1b84ff28c89e01d51a2312861b012599", null ],
    [ "test_initial_values", "test_2config_2address_8c.html#a4eef1151766ab7cf756da87b4923338a", null ],
    [ "test_string_set", "test_2config_2address_8c.html#a6481c1ea806f91ee55dacd7bfe01acd8", null ],
    [ "test_string_get", "test_2config_2address_8c.html#a85fd1e4e98bab2f2185f2d6cb270d14b", null ],
    [ "test_native_set", "test_2config_2address_8c.html#ab5935bf604cc953008fde261e13163b8", null ],
    [ "test_native_get", "test_2config_2address_8c.html#afdd5f411dfa0e2d1c7c7a2a9710182dd", null ],
    [ "test_reset", "test_2config_2address_8c.html#ac8ddbbe16d88cb9b096c15cf225cbeb3", null ],
    [ "test_validator", "test_2config_2address_8c.html#a906a32278ed761fb29743aebd54e5bac", null ],
    [ "dump_native", "test_2config_2address_8c.html#a3a25b594f616f113e6bc749efeb43531", null ],
    [ "test_inherit", "test_2config_2address_8c.html#a76541f7dea628989ebe0d44929067f64", null ],
    [ "config_address", "test_2config_2address_8c.html#a9efc5843bb346e494cdedcf6d276f01a", null ],
    [ "VarApple", "test_2config_2address_8c.html#a7dad748ad2ccbd42d6c80e1ec8c29496", null ],
    [ "VarBanana", "test_2config_2address_8c.html#ab9b28b66f20ead0d9a18f55e49bf9f95", null ],
    [ "VarCherry", "test_2config_2address_8c.html#a077f43131f1b4dec72cf0e5374b407a4", null ],
    [ "VarDamson", "test_2config_2address_8c.html#af7a38474e56af98b72c33180c508a0d5", null ],
    [ "VarElderberry", "test_2config_2address_8c.html#a312bfb4fb20781a5f18c4a0afc984e87", null ],
    [ "VarFig", "test_2config_2address_8c.html#ae5501c20ef83750a1f77e779b5737320", null ],
    [ "VarGuava", "test_2config_2address_8c.html#ae281039a0567e92df01c9a2baae92240", null ],
    [ "VarHawthorn", "test_2config_2address_8c.html#a031d2779fc2391dc7dd8ceb865a8f8a2", null ],
    [ "VarIlama", "test_2config_2address_8c.html#a4b723e4cf7797516c254ab71e16a754e", null ],
    [ "VarJackfruit", "test_2config_2address_8c.html#a2226455742204a8b04ed68698788e8f2", null ],
    [ "VarKumquat", "test_2config_2address_8c.html#a4cca86fec2a1e89972d53b1fb55e63f9", null ],
    [ "VarLemon", "test_2config_2address_8c.html#a9afdb744f65f2698da795d82ff7f4034", null ],
    [ "VarMango", "test_2config_2address_8c.html#ac815d37fe3ad1100d8fdc35f5d62cd94", null ],
    [ "VarNectarine", "test_2config_2address_8c.html#a98ac06e406333768e26b2906df3e5829", null ],
    [ "VarOlive", "test_2config_2address_8c.html#a7d9a0b328ab1bd4b529eb6450d6e91ec", null ],
    [ "VarPapaya", "test_2config_2address_8c.html#a55bc2ac44533b2cee5a4f5708eb16075", null ],
    [ "VarQuince", "test_2config_2address_8c.html#adde9f91d3ce42efe95053f1112be3dc9", null ],
    [ "Vars", "test_2config_2address_8c.html#a0ec6714905325f37533da49c4f419268", null ]
];