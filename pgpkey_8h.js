var pgpkey_8h =
[
    [ "PgpRing", "pgpkey_8h.html#a0aea3392606559730f643808f9a864e6", [
      [ "PGP_PUBRING", "pgpkey_8h.html#a0aea3392606559730f643808f9a864e6ab5ce327a30051bb49efdf113b51a1fc5", null ],
      [ "PGP_SECRING", "pgpkey_8h.html#a0aea3392606559730f643808f9a864e6aa5373b6a24d97bcf45a1acc335a8b35f", null ]
    ] ],
    [ "pgp_class_make_key_attachment", "pgpkey_8h.html#a2bdd0471e577ef56ced7214b5ab6c328", null ],
    [ "pgp_ask_for_key", "pgpkey_8h.html#afbaab02160e3bb6924aa118d26dc416b", null ],
    [ "pgp_getkeybyaddr", "pgpkey_8h.html#ae6f2683e100e9d80c5b35f8181b03474", null ],
    [ "pgp_getkeybystr", "pgpkey_8h.html#aa82025cf02aa8e7738d82ffe4463ba1f", null ]
];