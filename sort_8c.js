var sort_8c =
[
    [ "perform_auxsort", "sort_8c.html#a1d69cc6b2dfd825821b49d541edf5ca1", null ],
    [ "compare_score", "sort_8c.html#a390f5c31335e6dc72685e8dd1e299028", null ],
    [ "compare_size", "sort_8c.html#a7cf80b8a951b8178ec682c25c30c2e70", null ],
    [ "compare_date_sent", "sort_8c.html#a09ee03bee88cb5262f229decf9deb7d2", null ],
    [ "compare_subject", "sort_8c.html#a12c1665fc8262c4d55b44771dec15e16", null ],
    [ "mutt_get_name", "sort_8c.html#a454decb89368ace74106e17470e11113", null ],
    [ "compare_to", "sort_8c.html#acc5dc55cc415752c98d5ba8c00e55d9e", null ],
    [ "compare_from", "sort_8c.html#af82e3c226b7107de5ac93fcdb2e44c95", null ],
    [ "compare_date_received", "sort_8c.html#a8e36fd286ad8fec6518eea48af25e94d", null ],
    [ "compare_order", "sort_8c.html#a1c7b9b119be11c53e5c008ebf82862d4", null ],
    [ "compare_spam", "sort_8c.html#a99aa24b4aed29278d9933ac08668adae", null ],
    [ "compare_label", "sort_8c.html#aa1905fcf29c9728a25c85565b1b714a6", null ],
    [ "mutt_get_sort_func", "sort_8c.html#a86d2a4de61fab225b10c9e9a7bae01db", null ],
    [ "mutt_sort_headers", "sort_8c.html#aa99d4b590a29f24c0950934c15be6a83", null ],
    [ "ReverseAlias", "sort_8c.html#abdff29c6023f9dd2fb93d2889391ac6e", null ],
    [ "AuxSort", "sort_8c.html#a009c4f78eb0500b7ed1df68fe673de55", null ]
];