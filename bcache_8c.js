var bcache_8c =
[
    [ "BodyCache", "structBodyCache.html", "structBodyCache" ],
    [ "bcache_path", "bcache_8c.html#a3b622f605cf898b00c1f8adbdc27bb67", null ],
    [ "mutt_bcache_move", "bcache_8c.html#affefecb14cd62cc08faf0a6ba19b52c0", null ],
    [ "mutt_bcache_open", "bcache_8c.html#a291300794b8a7d98f451ca7ba335898d", null ],
    [ "mutt_bcache_close", "bcache_8c.html#af3a22202fb2135dfb1081cb6e6c5fecb", null ],
    [ "mutt_bcache_get", "bcache_8c.html#aecd453abbb62e97d92f9b39a469c8324", null ],
    [ "mutt_bcache_put", "bcache_8c.html#acf998afceb34defdd8d6be4b652ce729", null ],
    [ "mutt_bcache_commit", "bcache_8c.html#a5125c4a660fa9267a94153695c44a4f0", null ],
    [ "mutt_bcache_del", "bcache_8c.html#a81a73e04de0e74abec304abb57f9a21a", null ],
    [ "mutt_bcache_exists", "bcache_8c.html#a0fd8cd0e88c27788e6268674c71c7879", null ],
    [ "mutt_bcache_list", "bcache_8c.html#a0850e4f9575cfd0a2e35339873804c21", null ],
    [ "MessageCachedir", "bcache_8c.html#a48a4e1a1ac4d749f14ab9e45ddc2afc3", null ]
];