var pgplib_8h =
[
    [ "PgpUid", "structPgpUid.html", "structPgpUid" ],
    [ "PgpKeyInfo", "structPgpKeyInfo.html", "structPgpKeyInfo" ],
    [ "pgp_pkalgbytype", "pgplib_8h.html#aa7eff15366fb0874ba66c05b6d656fd3", null ],
    [ "pgp_copy_uids", "pgplib_8h.html#a32f7eec84948b7d944ffeabc2ab208ef", null ],
    [ "pgp_canencrypt", "pgplib_8h.html#ae3cf6bc1b74866665c1577970fffcb0a", null ],
    [ "pgp_cansign", "pgplib_8h.html#a7a081399a59a6168d26b8ed9963f1512", null ],
    [ "pgp_get_abilities", "pgplib_8h.html#a320f044d9a212dd7bcd9e81352201d24", null ],
    [ "pgp_free_key", "pgplib_8h.html#a3988a6bf657f175e77abe507c4917b5a", null ],
    [ "pgp_remove_key", "pgplib_8h.html#a16d1e87bc88ef769630e7a6c89998674", null ],
    [ "pgp_new_keyinfo", "pgplib_8h.html#a9dbd5f8092e073faae8a11f761fa8c04", null ]
];