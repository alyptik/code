var config =
[
    [ "Type: Email address", "config-address.html", null ],
    [ "Type: Boolean", "config-bool.html", null ],
    [ "Type: Command", "config-command.html", null ],
    [ "Dump all the config", "config-dump.html", null ],
    [ "Type: Long", "config-long.html", null ],
    [ "Type: Mailbox types", "config-magic.html", null ],
    [ "Type: Multi-byte character table", "config-mbtable.html", null ],
    [ "Type: Number", "config-number.html", null ],
    [ "Type: Path", "config-path.html", null ],
    [ "Type: Quad-option", "config-quad.html", null ],
    [ "Type: Regular expression", "config-regex.html", null ],
    [ "Config Set", "config-set.html", null ],
    [ "Type: Sorting", "config-sort.html", null ],
    [ "Type: String", "config-string.html", null ]
];