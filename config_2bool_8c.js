var config_2bool_8c =
[
    [ "bool_string_set", "config_2bool_8c.html#a3f0f8081b626af5ea57f2210b6f13927", null ],
    [ "bool_string_get", "config_2bool_8c.html#adcc1fd1ca971eacff907f8e5f08e6a15", null ],
    [ "bool_native_set", "config_2bool_8c.html#a8c62e12d9f8415ec172ca96cdf8d83f1", null ],
    [ "bool_native_get", "config_2bool_8c.html#add3e84a22709e9a0711b174a2d8201e8", null ],
    [ "bool_reset", "config_2bool_8c.html#ad3c45cb204d1372625fa72c9f00fa2db", null ],
    [ "bool_init", "config_2bool_8c.html#a95a976b51db5924fb9614cc41d4fed70", null ],
    [ "bool_he_toggle", "config_2bool_8c.html#a0c3d1d3762a3c3fb7815a3f3420a6a1e", null ],
    [ "bool_str_toggle", "config_2bool_8c.html#ae88becdf3a9af7ec837dfbdc9f7088ee", null ],
    [ "bool_values", "config_2bool_8c.html#ad0ca05587730a3ff8112e556024c6cbe", null ]
];