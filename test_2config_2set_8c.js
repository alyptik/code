var test_2config_2set_8c =
[
    [ "TEST_NO_MAIN", "test_2config_2set_8c.html#a1b84ff28c89e01d51a2312861b012599", null ],
    [ "dummy_string_set", "test_2config_2set_8c.html#ae052e1d25690b3f6b9348c685fb9292e", null ],
    [ "dummy_string_get", "test_2config_2set_8c.html#a72c2f1f2a2bed34fe2a414bc624a4d89", null ],
    [ "dummy_native_set", "test_2config_2set_8c.html#a350929483edbb409459e33be7e970537", null ],
    [ "dummy_native_get", "test_2config_2set_8c.html#ad328138c710517ae69059fd1d9e68036", null ],
    [ "dummy_reset", "test_2config_2set_8c.html#aeb34633d93685b7a4e2802c332c7383f", null ],
    [ "dummy_destroy", "test_2config_2set_8c.html#ac810c6408db283be1e17289e823c7a11", null ],
    [ "config_set", "test_2config_2set_8c.html#a9df85117efed404dbec10443a902d5dd", null ],
    [ "VarApple", "test_2config_2set_8c.html#a5fa3dd978cfa4545f774b9766c5dde09", null ],
    [ "VarBanana", "test_2config_2set_8c.html#aba4f103ddf6539edad683594c3838af2", null ],
    [ "Vars", "test_2config_2set_8c.html#a0ec6714905325f37533da49c4f419268", null ]
];