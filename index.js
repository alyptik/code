var index =
[
    [ "Address book handling aliases", "addrbook.html", null ],
    [ "String auto-completion routines", "complete.html", null ],
    [ "Compressed mbox local mailbox type", "compress.html", null ],
    [ "Duplicate the structure of an entire email", "copy.html", null ],
    [ "Prepare an email to be edited", "editmsg.html", null ],
    [ "GUI ask the user to enter a string", "enter.html", null ],
    [ "Pass files through external commands (filters)", "filter.html", null ],
    [ "Manipulate the flags in an email header", "flags.html", null ],
    [ "Parse and execute user-defined hooks", "hook.html", null ],
    [ "Command line processing", "main.html", null ],
    [ "Account object used by POP and IMAP", "mutt_account.html", null ],
    [ "Mutt Logging", "mutt_logging.html", null ],
    [ "Signal handling", "mutt_signal.html", null ],
    [ "NeoMutt connections", "mutt_socket.html", null ],
    [ "Window management", "window.html", null ],
    [ "Mailbox multiplexor", "mx.html", null ],
    [ "Save/restore and GUI list postponed emails", "postpone.html", null ],
    [ "Progress bar", "progress.html", null ],
    [ "GUI handle the resizing of the screen", "resize.html", null ],
    [ "RFC1524 Mailcap routines", "rfc1524.html", null ],
    [ "Wrapper for vasprintf()", "safe_asprintf.html", null ],
    [ "GUI display the mailboxes in a side panel", "sidebar.html", null ],
    [ "GUI display a user-configurable status line", "status.html", null ],
    [ "Execute external programs", "system.html", null ],
    [ "Set the terminal title/icon", "terminal.html", null ],
    [ "Display version and copyright about NeoMutt", "version.html", null ]
];