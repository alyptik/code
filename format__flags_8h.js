var format__flags_8h =
[
    [ "format_t", "format__flags_8h.html#a5df6d6e54e4b7405dd87ee388d6365ba", null ],
    [ "FormatFlag", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2", [
      [ "MUTT_FORMAT_FORCESUBJ", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2ae381ffb1d56f107335ef8f8749305a2e", null ],
      [ "MUTT_FORMAT_TREE", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2a8f07901e24df987d2af491f68d452b5c", null ],
      [ "MUTT_FORMAT_MAKEPRINT", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2ac272ef661b4498dc31a6c25d7ad81426", null ],
      [ "MUTT_FORMAT_OPTIONAL", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2af2d8536087d5168ce24d2aa7e71823cb", null ],
      [ "MUTT_FORMAT_STAT_FILE", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2a789083cceb1521181e9f9db173ad3ecf", null ],
      [ "MUTT_FORMAT_ARROWCURSOR", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2a61a54fb63ca7159a4856f1a44f3579c8", null ],
      [ "MUTT_FORMAT_INDEX", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2a57aaa32ba7ba71edde3e1a73ad376ac4", null ],
      [ "MUTT_FORMAT_NOFILTER", "format__flags_8h.html#ac33b2bd7bd59b88e7d9e43aa341cfcb2a2373fe62d703ac0da61308e94fe86049", null ]
    ] ]
];