var structAccount =
[
    [ "user", "structAccount.html#a89b88acb20a51660ca4d3b43d619e9dc", null ],
    [ "login", "structAccount.html#a74692bfa29daf559b755f07d6e179481", null ],
    [ "pass", "structAccount.html#a4f9068c078506a84a27a6d5ab10e80dd", null ],
    [ "host", "structAccount.html#a73f424a36aaa0e07e1ba3dda910fb3c6", null ],
    [ "port", "structAccount.html#afb8264fc1ecaa131de0e206914620b02", null ],
    [ "type", "structAccount.html#aabc5bed74588d8fd5c03bb80568946cf", null ],
    [ "flags", "structAccount.html#a3c5b681795d0f4a3f170bd8968f6db2f", null ],
    [ "name", "structAccount.html#a32c8a82b92146b6c614c5bdd3228e176", null ],
    [ "cs", "structAccount.html#ad903e0ebd524b0a51bed226f67609966", null ],
    [ "var_names", "structAccount.html#aaeb0f71a0f0fd6dbd2b43926b8042aa6", null ],
    [ "num_vars", "structAccount.html#a7b85125f1dd15255af7228f335154096", null ],
    [ "vars", "structAccount.html#ab0a328847919d7b5f725ab07dbfe8c25", null ]
];