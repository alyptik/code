var conn =
[
    [ "Connection Global Variables", "conn_globals.html", null ],
    [ "DNS lookups", "conn_getdomain.html", null ],
    [ "Low-level socket handling", "conn_raw.html", null ],
    [ "SASL authentication support", "conn_sasl.html", null ],
    [ "SASL plain authentication support", "conn_sasl_plain.html", null ],
    [ "Low-level socket handling", "conn_socket.html", null ],
    [ "Handling of OpenSSL encryption", "conn_ssl.html", null ],
    [ "Handling of GnuTLS encryption", "conn_ssl_gnutls.html", null ],
    [ "Support for network tunnelling", "conn_tunnel.html", null ]
];