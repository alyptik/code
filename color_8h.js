var color_8h =
[
    [ "ci_start_color", "color_8h.html#ac4e2c2714a5b5f264ef8cca7252002b3", null ],
    [ "mutt_alloc_color", "color_8h.html#a8b6e627eac23444ae14028ffe6ac4342", null ],
    [ "mutt_combine_color", "color_8h.html#a8fc9a723ea6647fc318d79d48b3f9adb", null ],
    [ "mutt_free_color", "color_8h.html#a2795da7fac706eeabb7a87ce52e2a926", null ],
    [ "mutt_free_colors", "color_8h.html#a796161759dd18e54fdc0b05d91b8a09c", null ],
    [ "mutt_parse_color", "color_8h.html#aad27e5705428c34d65b0ce55d5676689", null ],
    [ "mutt_parse_mono", "color_8h.html#a11b12814b115ecfd4390236e57ca4bb5", null ],
    [ "mutt_parse_uncolor", "color_8h.html#a3c0406295510c7f038822f53d1d4bae6", null ],
    [ "mutt_parse_unmono", "color_8h.html#a14377e0b6eecfba98e5cd5adb38b2669", null ]
];