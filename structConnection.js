var structConnection =
[
    [ "TAILQ_ENTRY", "structConnection.html#ac91f9a4f2977d40bfb91855acc0b3911", null ],
    [ "account", "structConnection.html#ac149f557906d987bd60845681803f0a0", null ],
    [ "ssf", "structConnection.html#a577e30cca37f125532cc73d4cbf51dcb", null ],
    [ "data", "structConnection.html#aedd67a1038db9b1a6025b4436a29c291", null ],
    [ "inbuf", "structConnection.html#abde20317141026670970092827259373", null ],
    [ "bufpos", "structConnection.html#a6c86ddad667d9db254dff142ceed42dd", null ],
    [ "fd", "structConnection.html#af805fad417c8fa7c5079ef741d8233e2", null ],
    [ "available", "structConnection.html#aa3ca1bfc7dba70f1eaf391eee9701330", null ],
    [ "sockdata", "structConnection.html#a2baa91d494f612c8bf43302bb96543f0", null ],
    [ "conn_read", "structConnection.html#ae299144310b938edaa341c973759948c", null ],
    [ "conn_write", "structConnection.html#a63e4d985dfe36e843463f47f35550109", null ],
    [ "conn_open", "structConnection.html#a6b9e5a08ec7c471f3fac9870829e6793", null ],
    [ "conn_close", "structConnection.html#a347b67a69529967654d4129b2d396a27", null ],
    [ "conn_poll", "structConnection.html#a3065db25b030883405792a81fd14555c", null ]
];