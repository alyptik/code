var envelope_8h =
[
    [ "Envelope", "structEnvelope.html", "structEnvelope" ],
    [ "mutt_env_cmp_strict", "envelope_8h.html#ae2a0adc41fba1815de39e238ed9c129a", null ],
    [ "mutt_env_free", "envelope_8h.html#a95a982abab9f28dc46d9a5da0b64ae4c", null ],
    [ "mutt_env_merge", "envelope_8h.html#a16aeda1ab6739eac80327f1ef797ffc7", null ],
    [ "mutt_env_new", "envelope_8h.html#a51f04365c2b348d829e7cd3710c55b5b", null ],
    [ "mutt_env_to_intl", "envelope_8h.html#a3a4de78d4202ee4ae94fa1d42a843d42", null ],
    [ "mutt_env_to_local", "envelope_8h.html#ac69fcedc4bf1e63a975f353396848bf8", null ]
];