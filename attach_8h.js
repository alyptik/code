var attach_8h =
[
    [ "AttachPtr", "structAttachPtr.html", "structAttachPtr" ],
    [ "AttachCtx", "structAttachCtx.html", "structAttachCtx" ],
    [ "mutt_actx_add_attach", "attach_8h.html#a1ac389e189d5c0cbb84711076812f103", null ],
    [ "mutt_actx_add_body", "attach_8h.html#a0de4bc51c9c5a28627f56ee542661e58", null ],
    [ "mutt_actx_add_fp", "attach_8h.html#abf8d6e3611faf77712e4e4066a0e198f", null ],
    [ "mutt_actx_free", "attach_8h.html#ac5399e3977a61d2e4b0364a0541a227d", null ],
    [ "mutt_actx_free_entries", "attach_8h.html#acdd8f21b02cf0b5c2ee6d99b7abf86f8", null ]
];