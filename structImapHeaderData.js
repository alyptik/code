var structImapHeaderData =
[
    [ "read", "structImapHeaderData.html#a364cc396517521c52f593968529729ce", null ],
    [ "old", "structImapHeaderData.html#af3d7cdf3839cef74c80e01ae525d25fd", null ],
    [ "deleted", "structImapHeaderData.html#ac30a52962eb657f2ee70a8e3f5f5bff1", null ],
    [ "flagged", "structImapHeaderData.html#a68421757947bec99e92182cd02023209", null ],
    [ "replied", "structImapHeaderData.html#a0ec82ceed8d3357c9f07ae42c1d7b4b6", null ],
    [ "changed", "structImapHeaderData.html#a206c08dc5e2506bc5d801ea0c017752d", null ],
    [ "parsed", "structImapHeaderData.html#a194228c8b7ce27cc4076bfde990bfa99", null ],
    [ "uid", "structImapHeaderData.html#aa0abba94c9917606c4ffab2dc293ca0f", null ],
    [ "msn", "structImapHeaderData.html#a59de2328dd31f54b83d2833225e2c684", null ],
    [ "flags_system", "structImapHeaderData.html#a98bf726158788fc7619e893be239d905", null ],
    [ "flags_remote", "structImapHeaderData.html#a41661611a145feb01e5e189fbdb64fb3", null ]
];