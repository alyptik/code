var curs__main_8h =
[
    [ "index_color", "curs__main_8h.html#a5832769943da43d891ebf56c0fd788b8", null ],
    [ "index_make_entry", "curs__main_8h.html#ae7ea32a946bde627b935602e2948023b", null ],
    [ "mutt_draw_statusline", "curs__main_8h.html#a180d846161e904fbcde10154790e49c4", null ],
    [ "mutt_index_menu", "curs__main_8h.html#af7d9d8649ad205788c9af1bbf871207f", null ],
    [ "mutt_set_header_color", "curs__main_8h.html#ab0b068f615db8886ab306cbd8a297e01", null ],
    [ "update_index", "curs__main_8h.html#a40e2ad7230ec2a26fba0da55e8987eed", null ],
    [ "ChangeFolderNext", "curs__main_8h.html#af74b08c6d3ffe0b8500667c8fd92d415", null ],
    [ "CollapseAll", "curs__main_8h.html#af9fea95f3cb7ab1d6f84031ae3b2712c", null ],
    [ "CollapseFlagged", "curs__main_8h.html#a9eaf077a1aca7a697f7cfe3672d2f9f4", null ],
    [ "CollapseUnread", "curs__main_8h.html#a4d72694d96a9c78f6a3f444f54a23872", null ],
    [ "MarkMacroPrefix", "curs__main_8h.html#ad6ad25af916149ab39989acbb7c40063", null ],
    [ "PgpAutoDecode", "curs__main_8h.html#a683aa27c7eb43ee63d018bd015de243e", null ],
    [ "UncollapseJump", "curs__main_8h.html#af8d0f3a18930879faebd81a903e692b2", null ],
    [ "UncollapseNew", "curs__main_8h.html#a2250e94890a0fc21b31a81c5be3f045d", null ]
];