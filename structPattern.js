var structPattern =
[
    [ "op", "structPattern.html#aa8bd5e633c1911b5c6fda6812479085a", null ],
    [ "not", "structPattern.html#a1d987d967a4b09f4a219328b71972d0f", null ],
    [ "alladdr", "structPattern.html#a5e1d1d31e5579b98a7c6b5eec7aff3e1", null ],
    [ "stringmatch", "structPattern.html#ac4e988fa21eb261f7aaaa9eae847de91", null ],
    [ "groupmatch", "structPattern.html#af1fa8219f7dc92e209b45c2b3e43c717", null ],
    [ "ign_case", "structPattern.html#a62f96e70e91227a7f62ff26d0e630a3b", null ],
    [ "isalias", "structPattern.html#a4ba0e77440668a0f79f09622fcba22b0", null ],
    [ "min", "structPattern.html#abea506d98549ecf2125399c6ec4768c8", null ],
    [ "max", "structPattern.html#a0d4d142b9ed06d4180dec28505da73ca", null ],
    [ "next", "structPattern.html#aff21021e06d9cb36292f504ce7f89a0f", null ],
    [ "child", "structPattern.html#a2f438209a55a00329bde4bd237b8ceec", null ],
    [ "regex", "structPattern.html#a31a5db2d7fccfc8eccb942932b4937b4", null ],
    [ "g", "structPattern.html#a7a5e4259a0107341f27f1f5aafc61b69", null ],
    [ "str", "structPattern.html#a502bcf07280750bacbba15073bee2aa6", null ],
    [ "p", "structPattern.html#af82f1223193fb4870c0a6f65a1847740", null ]
];